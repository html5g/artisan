import 'package:flutter/material.dart';

class KeepWork extends StatefulWidget {
  const KeepWork({super.key});

  @override
  State<KeepWork> createState() => _KeepWorkState();
}

class _KeepWorkState extends State<KeepWork>
    with AutomaticKeepAliveClientMixin {
  @override
  Widget build(BuildContext context) {
    super.build(context);
    return const Text('KeepWork');
  }

  @override
  bool get wantKeepAlive => true;
}
