import 'package:flutter/material.dart';

class KeepUser extends StatefulWidget {
  const KeepUser({super.key});

  @override
  State<KeepUser> createState() => _KeepUserState();
}

class _KeepUserState extends State<KeepUser>
    with AutomaticKeepAliveClientMixin {
  @override
  Widget build(BuildContext context) {
    super.build(context);
    return const Text('KeepUser');
  }

  @override
  bool get wantKeepAlive => true;
}
