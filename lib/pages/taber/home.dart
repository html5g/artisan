import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
// ignore: depend_on_referenced_packages
import 'package:flutter_swiper_null_safety/flutter_swiper_null_safety.dart';
import 'package:get/get.dart';
import 'package:tdesign_flutter/tdesign_flutter.dart';
import '/style/style.dart';
import '/config/permission.dart';
import '/assembly/cachedimage.dart';
import '/r.dart';
import '/assembly/urrefresh.dart';

class KeepHome extends StatefulWidget {
  const KeepHome({super.key});
  @override
  State<KeepHome> createState() => _KeepHomeState();
}

class _KeepHomeState extends State<KeepHome>
    with AutomaticKeepAliveClientMixin, WidgetsBindingObserver {
  RxString adddres = ''.obs;

  /// 轮播图片
  final List<String> _barList = [
    R.assetsImgIndexBanner,
    R.assetsImgIndexBannerOne,
    R.assetsImgIndexBannerTwo,
    R.assetsImgIndexBannerThree
  ];

  /// 金刚区数据
  final List<Map<String, String>> _gridList = [
    {'title': 'homeProject'.tr, 'url': R.assetsImgIndexProject, 'path': ''},
    {'title': 'homeEpoxy'.tr, 'url': R.assetsImgIndexEpoxy, 'path': ''},
    {'title': 'homeData'.tr, 'url': R.assetsImgIndexData, 'path': ''},
    {'title': 'homeBoard'.tr, 'url': R.assetsImgIndexBoard, 'path': ''},
    {'title': 'homeShare'.tr, 'url': R.assetsImgIndexShare, 'path': ''},
    {'title': 'homeRecruit'.tr, 'url': R.assetsImgIndexRecruit, 'path': ''},
    {'title': 'homeMaterial'.tr, 'url': R.assetsImgIndexMaterial, 'path': ''},
    {'title': 'homeFactory'.tr, 'url': R.assetsImgIndexFactory, 'path': ''},
    {'title': 'homeApprove'.tr, 'url': R.assetsImgIndexApprove, 'path': ''},
    {'title': 'homeReport'.tr, 'url': R.assetsImgIndexReport, 'path': ''},
    {'title': 'homeStudy'.tr, 'url': R.assetsImgIndexStudy, 'path': ''},
    {'title': 'homeCamera'.tr, 'url': R.assetsImgIndexStudy, 'path': '/camera'},
  ];

  /// 轮播是否停止动画
  final autoplay = true;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.resumed) {
      // 子组件即将进入屏幕
      print('Child is about to come on screen');
    } else if (state == AppLifecycleState.inactive) {
      // 子组件即将离开屏幕
      print('Child is about to go off screen');
    }
  }

  List<Widget> _instGridViewData() {
    List<Widget> list = [];
    for (var i = 0; i < _gridList.length; i++) {
      list.add(GestureDetector(
        onTap: () {
          Get.toNamed('${_gridList[i]['path']}');
        },
        child: Column(children: [
          CachedImage(
            imageUrl: '${_gridList[i]['url']}',
            fit: BoxFit.cover,
            height: 24.w,
            width: 24.w,
          ),
          SizedBox(height: Style.paddingSix),
          Text('${_gridList[i]['title']}',
              style: TextStyle(
                fontSize: Style.fontSizeTwelve,
              ))
        ]),
      ));
    }
    return list;
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return UrRefresh(
      onRefresh: () async {
        // 下拉刷新逻辑
        print('object');
      },
      onLoad: () async {
        // 上拉加载逻辑
        print('object');
      },
      child: CustomScrollView(
        slivers: [
          // 轮播
          SliverFixedExtentList(
              delegate:
                  SliverChildBuilderDelegate((BuildContext context, int index) {
                return Container(
                  color: Style.green,
                  padding: EdgeInsets.fromLTRB(Style.padding,
                      Style.paddingThree, Style.padding, Style.paddingThree),
                  child: Swiper(
                    autoplay: autoplay,
                    itemCount: _barList.length,
                    loop: true,
                    pagination: const SwiperPagination(
                        alignment: Alignment.bottomCenter,
                        builder: TDSwiperPagination.dotsBar),
                    itemBuilder: (BuildContext context, int index) {
                      return CachedImage(
                        fit: BoxFit.fitWidth,
                        imageUrl: _barList[index],
                      );
                    },
                  ),
                );
              }, childCount: 1),
              itemExtent: 100.h),
          // 消息通知
          SliverFixedExtentList(
              delegate:
                  SliverChildBuilderDelegate((BuildContext context, int index) {
                return Container(
                    decoration: BoxDecoration(
                      gradient: LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        colors: [
                          Style.green,
                          Style.white,
                        ],
                      ),
                    ),
                    padding: EdgeInsets.fromLTRB(
                        Style.padding, 0, Style.padding, Style.paddingSix),
                    child: Container(
                      width: double.infinity,
                      decoration: BoxDecoration(
                          color: Style.white,
                          borderRadius:
                              BorderRadius.circular(Style.borderRadiusFive)),
                      child: Padding(
                        padding: EdgeInsets.fromLTRB(Style.padding,
                            Style.paddingSix, Style.padding, Style.paddingSix),
                        child: Row(
                          children: [
                            Expanded(
                                flex: 3,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Expanded(
                                        flex: 1,
                                        child: Icon(
                                          Icons.notifications_none,
                                          size: Style.fontSizeSixteen.sp,
                                          color: Style.green,
                                        )),
                                    Expanded(
                                        flex: 10,
                                        child: Text(
                                          'more'.tr,
                                          style: TextStyle(
                                              color: Style.green,
                                              fontSize:
                                                  Style.fontSizeTwelve.sp),
                                        )),
                                  ],
                                )),
                            Expanded(
                                flex: 1,
                                child: Row(
                                  children: [
                                    Expanded(
                                        flex: 3,
                                        child: Text(
                                          'more'.tr,
                                          textAlign: TextAlign.right,
                                          style: TextStyle(
                                              color: Style.lightGray,
                                              fontSize:
                                                  Style.fontSizeTwelve.sp),
                                        )),
                                    Expanded(
                                        flex: 1,
                                        child: Icon(
                                          Icons.chevron_right_outlined,
                                          color: Style.lightGray,
                                          size: Style.fontSizeTwenty.sp,
                                        )),
                                  ],
                                ))
                          ],
                        ),
                      ),
                    ));
              }, childCount: 1),
              itemExtent: 28.h + Style.paddingSix.h),
          // 金刚区
          SliverFixedExtentList(
              delegate:
                  SliverChildBuilderDelegate((BuildContext context, int index) {
                return Container(
                    decoration: BoxDecoration(
                        color: Style.white,
                        borderRadius:
                            BorderRadius.circular(Style.borderRadiusFive)),
                    margin: EdgeInsets.fromLTRB(
                        Style.padding, 0, Style.padding, Style.paddingSix),
                    padding: EdgeInsets.only(
                        top: Style.padding, bottom: Style.padding),
                    child: GridView.count(
                      physics: const NeverScrollableScrollPhysics(),
                      crossAxisCount: 4, // 一列的个数
                      childAspectRatio: 4 / 3,
                      children: _instGridViewData(),
                    ));
              }, childCount: 1),
              itemExtent: 206),
          // 招工信息
          SliverFixedExtentList(
              delegate:
                  SliverChildBuilderDelegate((BuildContext context, int index) {
                return Container(
                    decoration: BoxDecoration(
                        color: Style.white,
                        borderRadius:
                            BorderRadius.circular(Style.borderRadiusFive)),
                    margin: EdgeInsets.fromLTRB(
                        Style.padding, 0, Style.padding, Style.paddingSix),
                    padding: EdgeInsets.only(
                        top: Style.padding, bottom: Style.padding),
                    child: GridView.count(
                      physics: const NeverScrollableScrollPhysics(),
                      crossAxisCount: 4, // 一列的个数
                      childAspectRatio: 4 / 3,
                      children: _instGridViewData(),
                    ));
              }, childCount: 3),
              itemExtent: 50),
        ],
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
