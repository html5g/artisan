import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '/style/style.dart';
import '/assembly/urrefresh.dart';

class KeepRecord extends StatefulWidget {
  const KeepRecord({super.key});

  @override
  State<KeepRecord> createState() => _KeepRecordState();
}

class _KeepRecordState extends State<KeepRecord>
    with AutomaticKeepAliveClientMixin {
  @override
  Widget build(BuildContext context) {
    super.build(context);
    return UrRefresh(
      onRefresh: () async {
        // 下拉刷新逻辑
        print('object');
      },
      onLoad: () async {
        // 上拉加载逻辑
        print('object');
      },
      child: CustomScrollView(
        slivers: [
          // 招工信息
          SliverFixedExtentList(
              delegate:
                  SliverChildBuilderDelegate((BuildContext context, int index) {
                return Container(
                    decoration: BoxDecoration(
                        color: Style.white,
                        borderRadius:
                            BorderRadius.circular(Style.borderRadiusFive)),
                    margin: EdgeInsets.fromLTRB(
                        Style.padding, 0, Style.padding, Style.paddingSix),
                    padding: EdgeInsets.only(
                        top: Style.padding, bottom: Style.padding),
                    child: GridView.count(
                      physics: const NeverScrollableScrollPhysics(),
                      crossAxisCount: 4, // 一列的个数
                      childAspectRatio: 4 / 3,
                      children: [],
                    ));
              }, childCount: 1),
              itemExtent: 50),
        ],
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
