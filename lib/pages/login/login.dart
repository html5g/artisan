import 'dart:async';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import '/storage/store.dart';
import '/common/login/login.dart';
import '/assembly/cachedimage.dart';
import '/assembly/urinput.dart';
import '/r.dart';
import '/style/style.dart';
import '/request/network.dart';
import '/config/toast.dart';
import '/config/limitation.dart';
import '/pages/middle/mydialog.dart';

class Login extends StatefulWidget {
  const Login({super.key});

  @override
  State<Login> createState() => _LoginState();
}

class _LoginState extends State<Login> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  String _username = '', _password = '';
  Store store = Get.put(Store()); // 实例化vuex(实例化控制器)
  RxInt _count = 0.obs; // 计数倒计时
  Timer? _time; // 计数实例
  final RxBool _timeShow = false.obs; // 是否开始计数
  final RxInt _workCode = 1.obs; // 是否是密码或者验证码登录
  final RxBool _visibility = true.obs; // 是否密码显示
  final RxBool _prohibit = false.obs; // 禁止重复点击
  RxString errorText = ''.obs;
  final _appid = '${Env.envConfig['app_auth_green']}'; // appid

  @override
  void initState() {
    super.initState();
    _count.value = store.codeCount;
  }

  @override
  void dispose() {
    if (_time != null) {
      _time?.cancel();
    }
    super.dispose();
  }

  // 隐私协议弹窗
  void _showPrivacy() async {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) {
          return MyDialog(
            confirm: _confirm,
            cancel: _cancel,
          ); // 自定义Dialog
        });
  }

  // 点击隐私同意的时候
  void _confirm() {
    store.setPrivacy(true);
    Navigator.pop(context);
    // _countPeriodic();
    _authCode();
  }

  // 点击隐私不同意的时候
  void _cancel() {
    Navigator.pop(context);
  }

  // 登录
  void _authCode() {
    // 没有选择隐私协议的时候
    if (!store.privacy.value) {
      _showPrivacy();
      return;
    }
    if (_prohibit.value) return;
    _prohibit.value = true;
    Limitation.debounce(
        duration: Limitation.defaultDuration(),
        tag: 'login',
        onExecute: () {
          login(data: {
            'phone': _username,
            'appId': _appid,
            'authId': _appid,
            'password': _password,
            'code': _password,
            'loginType': _workCode.value,
          }).then((res) {
            store.setToken(res['data']['accessToken']);
            store.setRefreshToken(res['data']['refreshToken']);
            _userInfoModel();
            _prohibit.value = false;
          }).catchError((err) {
            _prohibit.value = false;
            // if(err[])
            // 第一次维护基本信息
            if (err['errorCode'] == 'A0010' || err['errorCode'] == 'A0002') {
              Get.toNamed('/perfect', arguments: {'phone': _username});
            }
          });
        });
  }

  // 获取登录信息
  void _userInfoModel() {
    userInfo(data: {
      'appId': _appid,
    }).then((res) {
      store.setUserInfo(res['data']);
      Get.offAllNamed('/index', arguments: {'index': 1});
    }).catchError((err) {});
  }

  // 获取验证码
  void _userCode() {
    if (_prohibit.value) return;
    _prohibit.value = true;
    userCationCode(data: {
      'appId': _appid,
      'phone': _username,
    }).then((res) {
      _prohibit.value = false;
      ToastInfo.toast(msg: 'successful'.tr);
    }).catchError((err) {
      _prohibit.value = false;
    });
  }

  // 计数器和密码显示
  void _countPeriodic() {
    // 是密码框的时候
    if (_workCode.value == 1) {
      _visibility.value = !_visibility.value;
      return;
    }
    // 是验证码的时候
    if (_timeShow.value) return;
    if (_username.isEmpty) {
      ToastInfo.toast(msg: "${'enter'.tr}${'loginPhone'.tr}");
      return;
    }
    _userCode();
    _timeShow.value = true;
    _time = Timer.periodic(const Duration(seconds: 1), (timer) {
      if (_count <= 1) {
        _time?.cancel();
        _timeShow.value = false;
      } else {
        _count--;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: [
          Container(
            alignment: Alignment.center,
            margin: EdgeInsets.fromLTRB(Style.padding.w, 0, Style.padding.w, 0),
            child: Column(
              children: <Widget>[
                SizedBox(height: 84.h),
                CachedImage(
                  imageUrl: R.assetsImgLoginSystemLogo,
                  width: 149.w,
                  height: 36.h,
                ),
                SizedBox(height: 70.5.h),
                Form(
                  key: _formKey,
                  child: Column(
                    children: <Widget>[
                      // 手机号输入
                      UrInput(
                          hintText: 'loginPhone'.tr,
                          validator: (value) {
                            if (value!.isEmpty) {
                              return "${'enter'.tr}${'loginPhone'.tr}";
                            }
                            return null;
                          },
                          onSaved: (value) => _username = value,
                          onChanged: (value) => _username = value),

                      SizedBox(height: 24.h),
                      // 密码或者验证码
                      Obx(
                        () => UrInput(
                          hintText: _workCode.value == 1
                              ? 'loginPassword'.tr
                              : 'loginCode'.tr,
                          validator: (value) {
                            if (value!.isEmpty) {
                              return "${'enter'.tr}${_workCode.value == 1 ? 'loginPassword'.tr : 'loginCode'.tr}";
                            }
                            return null;
                          },
                          obscureText: _visibility.value,
                          suffixIcon: Stack(
                            alignment: Alignment.center,
                            children: [
                              Positioned(
                                child: TextButton(
                                  style: ButtonStyle(
                                    foregroundColor:
                                        MaterialStateProperty.all<Color>(
                                            Style.lightGray), // 文本颜色
                                  ),
                                  onPressed: _countPeriodic,
                                  child: _workCode.value == 1
                                      ? Icon(_visibility.value
                                          ? Icons.visibility_off
                                          : Icons.visibility)
                                      : Text(
                                          _timeShow.value
                                              ? '$_count S'
                                              : 'loginObtain'.tr,
                                          style: TextStyle(
                                              fontSize: Style.fontSizeTwelve),
                                        ),
                                ),
                              )
                            ],
                          ),
                          onChanged: (value) => _password = value,
                          onSaved: (value) => _password = value,
                        ),
                      ),

                      SizedBox(height: 20.h),
                      // 隐私协议
                      Obx(() => CheckboxListTile(
                            checkColor: Style.backGrey,
                            activeColor: Style.green,
                            contentPadding: const EdgeInsets.all(0),
                            side: const BorderSide(style: BorderStyle.solid),
                            title: RichText(
                              text: TextSpan(
                                  text: "loginAgree".tr,
                                  style: TextStyle(
                                      color: Style.lightGray,
                                      fontSize: Style.fontSizeTwelve),
                                  children: [
                                    TextSpan(
                                        text:
                                            "《${"name".tr}${"loginAgreement".tr}》《${"name".tr}${"loginPrivacy".tr}》",
                                        recognizer: TapGestureRecognizer()
                                          ..onTap =
                                              () => Get.toNamed('/privacy'),
                                        style: TextStyle(
                                            color: Style.green,
                                            fontSize: Style.fontSizeTwelve)),
                                    TextSpan(
                                      text:
                                          "，${"loginCraftsman".tr}${"name".tr}${"loginAccount".tr}",
                                      style: TextStyle(
                                          fontSize: Style.fontSizeTwelve),
                                    ),
                                  ]),
                            ),
                            value:
                                store.privacy.value, // 这里可以绑定一个bool变量来跟踪复选框的状态
                            controlAffinity: ListTileControlAffinity.leading,
                            onChanged: (value) {
                              // 复选框状态改变时的逻辑
                              store.privacy.value = value!;
                            },
                          )),
                      // 登录按钮
                      Obx(() => SizedBox(
                            width: double.infinity,
                            child: ElevatedButton(
                              style: ButtonStyle(
                                foregroundColor: MaterialStateProperty.all(
                                    Style.white), // 文字颜色
                                backgroundColor: MaterialStateProperty.all(
                                    Style.green.withOpacity(
                                        store.privacy.value ? 1 : .3)), // 背景颜色
                              ),
                              onPressed: () {
                                if (_formKey.currentState!.validate()) {
                                  _formKey.currentState?.save();
                                  // 处理登录逻辑
                                  _authCode();
                                  FocusManager.instance.primaryFocus?.unfocus();
                                }
                              },
                              child: Text('loginLogon'.tr),
                            ),
                          )),
                      // 切换验证码或者密码
                      Obx(
                        () => TextButton(
                            style: ButtonStyle(
                              foregroundColor: MaterialStateProperty.all<Color>(
                                  Style.lightGray), // 文本颜色
                            ),
                            onPressed: () {
                              _workCode.value = _workCode.value == 1 ? 0 : 1;
                              _visibility.value =
                                  _workCode.value == 1 ? true : false;
                              _formKey.currentState?.reset();
                              _username = '';
                              _password = '';
                            },
                            child: Text(
                              _workCode.value == 1
                                  ? "${'loginCode'.tr}${'loginLogon'.tr}"
                                  : "${'loginPassword'.tr}${'loginLogon'.tr}",
                              style: TextStyle(fontSize: Style.fontSizeTwelve),
                            )),
                      ),
                      Obx(() => Text(errorText.value))
                    ],
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
