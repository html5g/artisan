import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '/assembly/urappbar.dart';
import '/assembly/urfield.dart';
import '/style/style.dart';
import '/allocation/routeserver.dart';
import '/config/toast.dart';
import '/common/login/login.dart';
import '/request/network.dart';
import '/config/limitation.dart';
import '/storage/store.dart';

class Perfect extends StatefulWidget {
  const Perfect({super.key});

  @override
  State<Perfect> createState() => _PerfectState();
}

class _PerfectState extends State<Perfect> with RouteAware {
  Store store = Get.put(Store()); // 实例化vuex(实例化控制器)

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey();
  String _phone = '', _username = '', _code = '', _corpId = '';
  final RxString _corpName = ''.obs;
  final _appid = '${Env.envConfig['app_auth_green']}'; // appid

  @override
  void initState() {
    super.initState();
    print('------------------');
    _phone = Get.arguments?['phone'] ?? '';
  }

  /// 注册路由监听
  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    routeObserver.subscribe(this, ModalRoute.of(context) as PageRoute);
  }

  /// 注销路由监听
  @override
  void dispose() {
    super.dispose();
    routeObserver.unsubscribe(this);
  }

  /// 路由改变的时候 第一次不会触发
  @override
  void didPopNext() {
    super.didPopNext();
    print('Get.parametersGet.parametersGet.parameters');
  }

  /// 选择公司的时候，then是get.back返回的参数
  void _openDrawer() {
    ///
    Get.toNamed('/company')?.then((res) {
      if (res != null) {
        _corpName.value = res['cropName'];
        _corpId = res['id'];
      }
    });
  }

  //
  void _userRegister() {
    if (_username == '') {
      ToastInfo.toast(msg: "${'enter'.tr}${'perName'.tr}");
      return;
    }
    if (_corpId == '') {
      ToastInfo.toast(msg: "${'select'.tr}${'perCompany'.tr}");
      return;
    }
    Limitation.debounce(
        duration: Limitation.defaultDuration(),
        tag: 'register',
        onExecute: () {
          userRegister(data: {
            'appid': _appid,
            'name': _username,
            'phone': _phone,
            'invitationCode': _code,
            'createSource': 2,
          }).then((res) {
            print(res);
            print(res['data']['accessToken']);
            if (res['data']['accessToken'] != null) {
              store.setToken(res['data']['accessToken']);
              store.setRefreshToken(res['data']['refreshToken']);
              _userInfoModel();
            }
          }).catchError((err) {});
        });
  }

  // 获取登录信息
  void _userInfoModel() {
    userInfo(data: {
      'appId': _appid,
    }).then((res) {
      print('res');
      print(res);
      print('res');

      store.setUserInfo(res['data']);
      _userAddCorp();
    }).catchError((err) {});
  }

  // 加入企业
  void _userAddCorp() {
    userAddCorp(data: {
      'userId': store.userInfo['id'],
      'account': store.userInfo['account'],
      'name': _username,
      'corpName': _corpName.value,
      'corpId': _corpId,
    }).then((res) {
      Get.offAllNamed('/index', arguments: {'index': 1});
    }).catchError((err) {});
  }

  @override
  Widget build(BuildContext context) {
    // 当页面A被弹栈回来时，更新数据
    return Scaffold(
      key: _scaffoldKey,
      appBar: UrAppbar(
        title: 'perTitle'.tr,
      ),
      body: Stack(
        children: [
          Container(
            margin: EdgeInsets.fromLTRB(Style.padding, 0, Style.padding, 0),
            child: Column(children: [
              // 手机号
              UrField(
                initialValue: _phone,
                title: 'loginPhone'.tr,
                enabled: false,
              ),
              // 真实姓名
              UrField(
                hintText: 'enter'.tr,
                initialValue: _username,
                title: 'perName'.tr,
                onChanged: (text) => _username = text!,
              ),
              // 公司
              Obx(
                () => UrField(
                    hintText: 'select'.tr,
                    initialValue: _username,
                    title: 'perCompany'.tr,
                    enabled: false,
                    childWidget: TextButton(
                        style: ButtonStyle(
                          padding: MaterialStateProperty.all(
                              const EdgeInsets.symmetric(
                                  horizontal: 0, vertical: 0)),
                        ),
                        onPressed: _openDrawer,
                        child: Row(
                          children: [
                            Expanded(
                              child: Text(
                                textAlign: TextAlign.right,
                                _corpName.value == ''
                                    ? 'enter'.tr
                                    : _corpName.value,
                                style: TextStyle(
                                    color: _corpName.value == ''
                                        ? Style.lightGray
                                        : Style.black,
                                    fontSize: Style.fontSizeSixteen.sp,
                                    overflow: TextOverflow.ellipsis),
                              ),
                            ),
                            SizedBox(
                              width: 20,
                              height: 40,
                              child: Icon(
                                Icons.chevron_right,
                                color: Style.lightGray,
                              ),
                            ),
                          ],
                        ))),
              ),
              // 邀请码
              UrField(
                hintText: 'enter'.tr,
                initialValue: _code,
                title: 'perCode'.tr,
                onChanged: (text) => _code = text!,
              ),
            ]),
          ),
          Positioned(
            bottom: 0,
            left: 0.0,
            right: 0.0,
            child: Container(
              color: Style.white,
              child: TextButton(
                  style: ButtonStyle(
                      backgroundColor: MaterialStateProperty.all(Style.green)),
                  onPressed: _userRegister,
                  child: Text(
                    "confirm".tr,
                    style: TextStyle(color: Style.white),
                  )),
            ),
          )
        ],
      ),
    );
  }
}
