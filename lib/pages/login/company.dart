import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '/assembly/urcard.dart';
import '/assembly/urappbar.dart';
import '/style/style.dart';
import '/assembly/urinput.dart';
import '/common/login/login.dart';
import '/request/network.dart';
import '/assembly/urrefresh.dart';

class Company extends StatefulWidget {
  const Company({super.key});

  @override
  State<Company> createState() => _CompanyState();
}

class _CompanyState extends State<Company> {
  final RxMap _sendFrom = {
    'searchValue': '',
    'pageNo': 1,
    'pageSize': 10,
    'appid': '${Env.envConfig['app_auth_green']}'
  }.obs;
  final RxMap<String, dynamic> _company = {'id': ''}.obs;
  // 上拉加载
  final RxList _companyList = [].obs; //
  bool _isLoading = false; // 是否正在加载
  bool _refresh = false; // 是否海域数据
  final RxBool _sample = true.obs; // 骨架

  @override
  void initState() {
    super.initState();
    _userCompany();
  }

  Future<void> _loadMore() async {
    _sendFrom['pageNo'] = 1;
    _isLoading = false;
    _refresh = false;
    _companyList.value = [];
    _userCompany();
  }

  void _userCompany() {
    if (_isLoading) return;
    if (_refresh) return;
    _isLoading = true;
    userCorp(data: _sendFrom).then((res) {
      if (res['data']['items'] is List) {
        if (_sendFrom['pageNo'] == 1) {
          _companyList.value = res['data']['items'];
        } else {
          _companyList.addAll(res['data']['items']);
        }
        if (res['data']['hasNextPages']) {
          _sendFrom['pageNo']++;
        } else {
          _refresh = true;
        }
      }
      _isLoading = false;
      _sample.value = false;
    }).catchError((err) {
      _isLoading = false;
      _sample.value = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: UrAppbar(
          title: 'perCorp'.tr,
        ),
        body: Stack(children: [
          Obx(
            () => UrRefresh(
              onRefresh: () async {
                // 下拉刷新逻辑
                // ...
                // await Future.delayed(Duration(seconds: 2));
                _loadMore();
              },
              onLoad: () async {
                // 上拉加载逻辑
                _userCompany();
              },
              sample: _sample.value,
              child: ListView(
                children: [
                  Column(
                    children: [
                      // 搜索
                      UrCard(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Expanded(
                                flex: 4,
                                child: UrInput(
                                    textAlign: TextAlign.left,
                                    hintText: 'enter'.tr,
                                    onChanged: (value) =>
                                        _sendFrom['searchValue'] = value)),
                            Expanded(
                                child: TextButton(
                              onPressed: () {
                                _loadMore();
                              },
                              child: Text('搜索'),
                            ))
                          ],
                        ),
                      ),
                      Obx(
                        () => UrCard(
                          child: Column(
                            children: _companyList.map((item) {
                              return GestureDetector(
                                onTap: () {
                                  _company['id'] = item['id'];
                                  _company['cropName'] = item['cropName'];
                                },
                                child: Container(
                                    decoration: BoxDecoration(
                                        border: Border(
                                            bottom: BorderSide(
                                                color: Style.backGrey))),
                                    padding: EdgeInsets.fromLTRB(0,
                                        Style.paddingSix, 0, Style.paddingSix),
                                    // margin: EdgeInsets.fromLTRB(
                                    //     0,
                                    //     Style.paddingThree,
                                    //     0,
                                    //     Style.paddingThree),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Expanded(
                                            flex: 9,
                                            child: Text(
                                              item['cropName'],
                                              style: TextStyle(
                                                  color: item['id'] ==
                                                          _company['id']
                                                      ? Style.green
                                                      : Style.black),
                                            )),
                                        Expanded(
                                            child: Visibility(
                                                visible: item['id'] ==
                                                    _company['id'],
                                                child: Icon(
                                                  Icons.done,
                                                  color: Style.green,
                                                  size: Style.fontSizeEighteen,
                                                )))
                                      ],
                                    )),
                              );
                            }).toList(),
                          ),
                        ),
                      ),
                      const SizedBox(
                        height: 50,
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
          const SizedBox(
            height: 50,
          ),
          // 底部确认和取消按钮
          Obx(() => Positioned(
              bottom: 0,
              left: 0.0,
              right: 0.0,
              child: Visibility(
                visible: _company['id'] != '',
                child: Container(
                  color: Style.white,
                  child: TextButton(
                      style: ButtonStyle(
                          backgroundColor:
                              MaterialStateProperty.all(Style.green)),
                      onPressed: () {
                        Get.back(result: _company);
                      },
                      child: Text(
                        "confirm".tr,
                        style: TextStyle(color: Style.white),
                      )),
                ),
              )))
        ]));
  }
}
