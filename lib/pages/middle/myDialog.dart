import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '/style/style.dart';

/// 隐私协议弹窗

class MyDialog extends Dialog {
  final Function? confirm;
  final Function? cancel;
  const MyDialog({super.key, this.confirm, this.cancel});

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size; // 页面信息
    final double width = size.width * 0.8; // 弹出协议宽度
    final double height = size.height * 0.8; // 弹出协议高度
    const double viewHeight = 30; // 标题高度
    //协议说明文案
    String userPrivateProtocol =
        "我们一向尊重并会严格保护用户在使用本产品时的合法权益（包括用户隐私、用户数据等）不受到任何侵犯。\n2. 本协议（包括本文最后部分的隐私政策）是用户（包括通过各种合法途径获取到本产品的自然人、法人或其他组织机构，以下简称“用户”或“您”）与我们之间针对本产品相关事项最终的、完整的且排他的协议，并取代、合并之前的当事人之间关于上述事项的讨论和协议。\n3. 本协议将对用户使用本产品的行为产生法律约束力，您已承诺和保证有权利和能力订立本协议。\n4.用户开始使用本产品将视为已经接受本协议，请认真阅读并理解本协议中各种条款，包括免除和限制我们的免责条款和对用户的权利限制（未成年人审阅时应由法定监护人陪同），如果您不能接受本协议中的全部条款，请勿开始使用本产品";

    return Material(
        type: MaterialType.transparency, // 背景透明
        child: Center(
          child: Container(
            width: width,
            height: height,
            padding: EdgeInsets.all(Style.padding),
            decoration: BoxDecoration(
              color: Style.white,
              borderRadius: Style.radiusTwenty,
            ),
            child: Stack(
              children: [
                ListView(
                  padding: EdgeInsets.fromLTRB(0, viewHeight + Style.padding, 0,
                      viewHeight + Style.padding),
                  children: [
                    RichText(
                      text: TextSpan(
                          text: "1. 请您在使用本产品之前仔细阅读",
                          style: TextStyle(color: Style.lightGray),
                          children: [
                            TextSpan(
                                text: "《用户协议》",
                                recognizer: TapGestureRecognizer()
                                  ..onTap = () => Get.toNamed('/privacy'),
                                style: TextStyle(color: Style.green)),
                            const TextSpan(
                              text: "与",
                            ),
                            TextSpan(
                              text: "《隐私协议》",
                              recognizer: TapGestureRecognizer()
                                ..onTap = () => Get.toNamed('/privacy'),
                              style: TextStyle(color: Style.green),
                            ),
                            TextSpan(
                              text: userPrivateProtocol,
                            ),
                          ]),
                    )
                  ],
                ),
                Positioned(
                    top: -2.0,
                    left: 0.0,
                    right: 0.0,
                    child: Container(
                      width: double.infinity,
                      height: viewHeight,
                      alignment: Alignment.center,
                      // padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
                      decoration: BoxDecoration(
                        color: Style.white,
                      ),
                      child: Text(
                        '${'name'.tr}${'privacy'.tr}',
                        style: TextStyle(
                          fontSize: Style.fontSizeFifteen,
                        ),
                      ),
                    )),
                Positioned(
                    bottom: -12,
                    child: Container(
                      width: width,
                      decoration: BoxDecoration(
                        color: Style.white,
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          TextButton(
                              onPressed: () async {
                                cancel!();
                              },
                              child: Text(
                                'disagree'.tr,
                                style: TextStyle(color: Style.red),
                              )),
                          TextButton(
                              onPressed: () {
                                confirm!();
                              },
                              child: Text('agreewith'.tr)),
                        ],
                      ),
                    )),
              ],
            ),
          ),
        ));
  }
}
