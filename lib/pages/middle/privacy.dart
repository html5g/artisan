import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:webview_flutter/webview_flutter.dart';

class Privacy extends StatefulWidget {
  const Privacy({super.key});

  @override
  PrivacyState createState() => PrivacyState();
}

class PrivacyState extends State<Privacy> {
  late WebViewController controller;
  @override
  void initState() {
    controller = WebViewController()
      ..setJavaScriptMode(JavaScriptMode.unrestricted)
      ..setBackgroundColor(const Color(0x00000000))
      ..setNavigationDelegate(
        NavigationDelegate(
          onProgress: (int progress) {
            // Update loading bar.
          },
          onPageStarted: (String url) {},
          onPageFinished: (String url) {},
          onWebResourceError: (WebResourceError error) {},
          onNavigationRequest: (NavigationRequest request) {
            if (request.url.startsWith(
                'http://file.epumping.com/doc/AppPrivacyPolicy/GreenApp.html#/')) {
              return NavigationDecision.prevent;
            }
            return NavigationDecision.navigate;
          },
        ),
      )
      ..loadRequest(Uri.parse(
          'http://file.epumping.com/doc/AppPrivacyPolicy/GreenApp.html#/'));
    // ..setJavaScriptMode(JavaScriptMode.unrestricted)
    // ..addJavaScriptChannel('Report', onMessageReceived: (mesdsage) {})
    // ..loadHtmlString(
    //     'http://file.epumping.com/doc/AppPrivacyPolicy/GreenApp.html')
    // ..loadRequest(Uri.parse(
    //     'http://file.epumping.com/doc/AppPrivacyPolicy/GreenApp.html'));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('privacy'.tr),
        ),
        body: Column(
          children: [
            Expanded(
                child: WebViewWidget(
              controller: controller,
            ))
          ],
        ));
  }
}
