import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import '/pages/middle/mydialog.dart';
import '/style/style.dart';
import 'package:get/get.dart';
import '/storage/store.dart';
import 'dart:async';
import '/r.dart';

class Wait extends StatefulWidget {
  const Wait({super.key});

  @override
  State<Wait> createState() => _WaitState();
}

class _WaitState extends State<Wait> {
  Store store = Get.put(Store()); // 实例化vuex(实例化控制器)
  RxInt _count = 0.obs; // 计数倒计时
  late Timer time; // 计数实例
  @override
  void initState() {
    super.initState();
    // 初始化计数器时间
    _count.value = store.count;
    // 选择了隐私协议
    if (store.privacy.value) {
      _countPeriodic();
    } else {
      WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
        _showPrivacy();
      });
    }
  }

  @override
  void dispose() {
    super.dispose();
    time.cancel();
  }

  // 跳转首页
  void _jumpHome() {
    Get.offNamed('/index');
  }

  // 点击隐私同意的时候
  void _confirm() {
    store.setPrivacy(true);
    Navigator.pop(context);
    _countPeriodic();
  }

  // 点击隐私不同意的时候
  void _cancel() {
    SystemNavigator.pop();
  }

  // 计数器
  void _countPeriodic() {
    time = Timer.periodic(const Duration(seconds: 1), (timer) {
      if (_count <= 1) {
        time.cancel();
        _jumpHome();
      } else {
        _count--;
      }
    });
  }

  // 隐私协议弹窗
  void _showPrivacy() async {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) {
          return MyDialog(
            confirm: _confirm,
            cancel: _cancel,
          ); // 自定义Dialog
        });
  }

  @override
  Widget build(BuildContext context) {
    // 导航栏高度
    double navBarHeight = MediaQuery.of(context).padding.top;
    return Stack(children: [
      Container(
        width: double.infinity,
        height: double.infinity,
        decoration: const BoxDecoration(
            image: DecorationImage(
                image: AssetImage(R.assetsImgMiddleWait), fit: BoxFit.cover)),
      ),
      Positioned(
          top: navBarHeight + 24,
          right: 12,
          child: Container(
              width: 50.w,
              height: 25.h,
              alignment: Alignment.center,
              decoration: BoxDecoration(
                color: Style.middle,
                borderRadius: Style.radiusHundred,
                boxShadow: const [
                  BoxShadow(
                      color: Colors.black12,
                      offset: Offset(0, 0),
                      blurRadius: 5)
                ], // 阴影
              ),
              child: TextButton(
                  onPressed: _jumpHome,
                  style: ButtonStyle(
                    visualDensity: VisualDensity.compact,
                    minimumSize: MaterialStateProperty.all(const Size(50, 25)),
                  ),
                  child: Obx(
                    () => Text(
                      "${_count}s",
                      style: TextStyle(
                          fontSize: Style.fontSizeFifteen.sp,
                          color: Style.green),
                      textAlign: TextAlign.center,
                    ),
                  )))),
    ]);
  }
}
