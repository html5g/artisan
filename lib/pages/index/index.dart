import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '/storage/store.dart';
import '/style/style.dart';
import '/assembly/cachedimage.dart';
import '/r.dart';

import '/pages/taber/home.dart';
import '/pages/taber/record.dart';
import '/pages/taber/work.dart';
import '/pages/taber/user.dart';

class Index extends StatefulWidget {
  const Index({super.key, index = 0});

  @override
  State<Index> createState() => _IndexState();
}

class _IndexState extends State<Index> {
  final Store _store = Get.put(Store()); // 实例化vuex(实例化控制器)
  int _currentIndex = 0; // 激活哪一项
  late PageController _pageController;
  final List<Map> _navBarList = [
    {
      'appTitle': CachedImage(
        imageUrl: R.assetsImgLoginWhiteLogo,
        width: 94.5.w,
        height: 23.h,
      ),
      'backColor': Style.green,
    },
    {
      'appTitle': Text('appbarWorkText'.tr),
      'backColor': Style.green,
    },
    {
      'appTitle': Text('appbarPoking'.tr),
      'backColor': Style.green,
    },
    {
      'appTitle': Text('appbarUser'.tr),
      'backColor': Style.green,
    },
  ];
  final List<Widget> _widgetList = [
    const KeepHome(),
    const KeepRecord(),
    const KeepWork(),
    const KeepUser(),
  ];
  @override
  void initState() {
    super.initState();
    _currentIndex = _store.taberIndex.value; // Getx Obs
    _pageController =
        PageController(initialPage: _currentIndex, keepPage: true);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: _navBarList[_currentIndex]['appTitle'],
        backgroundColor: _navBarList[_currentIndex]['backColor'],
      ),
      bottomNavigationBar: BottomNavigationBar(
        selectedItemColor: Style.green, // 选择的颜色
        unselectedItemColor: Style.lightGray, // 没有选择的颜色
        type: BottomNavigationBarType.fixed,
        backgroundColor: Style.white, // 背景色
        currentIndex: _currentIndex, // 当前激活的元素
        onTap: (index) {
          setState(() {
            _currentIndex = index;
            _pageController.jumpToPage(index);
          });
        }, // 点击某一项回调元素
        items: [
          BottomNavigationBarItem(
              icon: const Icon(Icons.home), label: 'appbarHome'.tr),
          BottomNavigationBarItem(
              icon: const Icon(Icons.event_note_outlined),
              label: 'appbarWork'.tr),
          BottomNavigationBarItem(
              icon: const Icon(Icons.contacts_outlined),
              label: 'appbarPoking'.tr),
          BottomNavigationBarItem(
              icon: const Icon(Icons.perm_identity), label: 'appbarUser'.tr),
        ],
      ),
      body: PageView(
        scrollDirection: Axis.vertical,
        physics: const NeverScrollableScrollPhysics(),
        controller: _pageController,
        children: _widgetList,
      ),
    );
  }
}
