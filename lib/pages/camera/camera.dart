/// 自定义水印相机
library;

import 'dart:async';
import 'dart:io';
import 'dart:ui';
import 'package:artisan/style/style.dart';
import 'package:camera/camera.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:video_player/video_player.dart';
import 'package:get/get.dart';
import 'package:image_gallery_saver/image_gallery_saver.dart';
import 'package:video_watermark/video_watermark.dart';

import '/config/permission.dart';
import '/icons/modelicon.dart';
import '/config/limitation.dart';

class MyPainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    final paint = Paint()
      ..color = Colors.red
      ..style = PaintingStyle.fill;
    canvas.drawRect(Rect.fromLTWH(0, 0, size.width, size.height), paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }
}

/// Camera example home widget.
class Camera extends StatefulWidget {
  /// Default Constructor
  const Camera({super.key});

  @override
  State<Camera> createState() {
    return _CameraState();
  }
}

class _CameraState extends State<Camera> {
  // Counting pointers (number of user fingers on screen)、
  /// 定义相机实例
  late CameraController _controller;

  /// 获取相机实例
  List<CameraDescription> _cameras = <CameraDescription>[];

  /// true 前置摄像头
  final RxBool _direction = true.obs;

  /// 判断相机是否初始化完成
  bool _isInitialized = false;

  /// 定义相机闪光灯 1 表示没有开启 2 表示开启 3 表示自动  4 长亮
  final RxInt _flashInt = 1.obs;

  /// 相机是否在使用
  final RxBool _isTakingPicture = true.obs;

  /// 计数倒计时30秒
  RxInt _count = 3.obs;

  /// 计数实例
  Timer? _time;

  /// 拍照或者视频文件
  XFile? imageFile;

  /// 判断是拍照还是拍视频 默认拍照
  final RxBool _onFileRmine = true.obs;

  /// 播放视频实例
  VideoPlayerController? _videoController;
  VoidCallback? videoPlayerListener;

  /// 画布实例
  final globalKey = GlobalKey();

  @override
  void initState() {
    super.initState();

    /// 初始化相机
    _initializeCamera();
  }

  void _initializeCamera() async {
    /// 获取相机实例
    _cameras = await availableCameras();

    /// 赋值相机实例
    _controller = CameraController(
      _cameras.first,
      ResolutionPreset.max,
      enableAudio: false,
    );

    /// 初始化实例
    await _controller.initialize();

    /// 启动相机
    setState(() {
      _isInitialized = true;
    });

    /// 完成初始化
    _isTakingPicture.value = false;
  }

  /// 播放视频
  Future<void> _startVideoPlayer() async {
    if (imageFile == null) {
      return;
    }

    final VideoPlayerController vController = kIsWeb
        ? VideoPlayerController.networkUrl(Uri.parse(imageFile!.path))
        : VideoPlayerController.file(File(imageFile!.path));

    videoPlayerListener = () {
      if (_videoController != null) {
        // Refreshing the state to update video player with the correct ratio.
        setState(() {});
        _videoController!.removeListener(videoPlayerListener!);
      }
    };
    vController.addListener(videoPlayerListener!);
    await vController.setLooping(true);
    await vController.initialize();
    await _videoController?.dispose();
    setState(() {
      imageFile = null;
      _videoController = vController;
    });
    print('321321321323');
    await vController.play();
  }

  /// 相机拍照
  void _takePicture() {
    /// 禁止重复点击
    if (_isTakingPicture.value) return;
    Limitation.debounce(
        duration: Limitation.defaultDuration(),
        tag: 'camera-photography',
        onExecute: () async {
          _isTakingPicture.value = true;
          _onFileRmine.value = true;
          try {
            // 确保相机初始化完成
            if (!_controller.value.isInitialized) {
              throw Exception('相机尚未初始化！');
            }
            if (_controller.value.isTakingPicture) {
              // A capture is already pending, do nothing.
              throw Exception('相机正在被占用');
            }
            final XFile file = await _controller.takePicture();
            imageFile = file;
            _isTakingPicture.value = false;
          } catch (e) {
            _isTakingPicture.value = false;
          }
        });
  }

  /// 相机拍摄视频
  void _takePictureVideo() async {
    if (_isTakingPicture.value) {
      /// 再次点击直接生成视频
      stopVideoRecording();
      return;
    }
    Limitation.debounce(
        duration: Limitation.defaultDuration(),
        tag: 'camera-video',
        onExecute: () async {
          _isTakingPicture.value = true;
          _onFileRmine.value = false;
          try {
            // 确保相机初始化完成
            if (!_controller.value.isInitialized) {
              throw Exception('相机尚未初始化！');
            }

            if (_controller.value.isRecordingVideo) {
              // A recording is already started, do nothing.
              throw Exception('相机正在录制');
            }
            await _controller.startVideoRecording();

            /// 开始录制计时
            _time = Timer.periodic(const Duration(seconds: 1), (timer) {
              if (_count <= 1) {
                _time?.cancel();
                stopVideoRecording();
                _isTakingPicture.value = false;
              } else {
                _count--;
              }
            });
          } catch (e) {
            print(e);
            _isTakingPicture.value = false;
          }
        });
  }

  /// 停止视频
  void stopVideoRecording() async {
    _time?.cancel();
    _onFileRmine.value = true;
    try {
      if (!_controller.value.isRecordingVideo) {
        throw Exception('摄像头没有被占用');
      }
      XFile file = await _controller.stopVideoRecording();
      print('File!.path');
      print(file.path);
      print('File!.path');
      imageFile = file;
      _startVideoPlayer();
    } catch (e) {}
  }

  /// 相机闪光灯图标
  IconData _flashIcon() {
    /// 开启
    if (_flashInt.value == 2) {
      return ModelIcon.flashOn;
    }

    /// 自动
    if (_flashInt.value == 3) {
      return ModelIcon.flashAuto;
    }

    /// 长亮
    if (_flashInt.value == 4) {
      return ModelIcon.flashLight;
    }
    return ModelIcon.flashOff;
  }

  /// 切换闪光灯
  void _switchingFlash() {
    if (_isTakingPicture.value) return;
    Limitation.debounce(
        duration: Limitation.defaultDuration(),
        tag: 'flash',
        onExecute: () {
          print('object');
          if (_flashInt.value == 4) {
            _flashInt.value = 1;
          } else {
            _flashInt.value++;
          }

          /// 定义相机闪光灯 1 表示没有开启 2 表示开启 3 表示自动  4 长亮
          /// 关闭
          if (_flashInt.value == 1) {
            _controller.setFlashMode(FlashMode.off);
          }

          /// 开启
          if (_flashInt.value == 2) {
            _controller.setFlashMode(FlashMode.always);
          }

          /// 自动
          if (_flashInt.value == 1) {
            _controller.setFlashMode(FlashMode.auto);
          }

          /// 长亮
          if (_flashInt.value == 1) {
            _controller.setFlashMode(FlashMode.torch);
          }
        });
  }

  /// 切换摄像头
  void _switchingCamera() {
    if (_isTakingPicture.value) return;
    Limitation.debounce(
        duration: Limitation.defaultDuration(),
        tag: 'camera',
        onExecute: () {
          // 更新摄像头方向
          if (_cameras.length >= 2) {
            _direction.value = !_direction.value;
            if (_direction.value) {
              _controller.setDescription(_cameras[0]);
            } else {
              _controller.setDescription(_cameras[1]);
            }
          }
        });
  }

  void _switchingCameraVideo() {
    PermissionHandler.requestAlbum().then((value) {
      // List<String> commands = [
      //   '-i',
      //   '${value['data'][0].path}',
      //   '-i',
      //   'assets/img/index/bannerTwo.png',
      //   '-filter_complex',
      //   "overlay=W-w-10:H-h-10",
      //   '-c:v mpeg4',
      //   '/storage/emulated/0/Movies/VID_20240528_03147778555.mp4'
      // ];
      // FFmpegKit.execute(commands.join(' ')).then((session) async {
      //   final returnCode = await session.getReturnCode();

      //   if (ReturnCode.isSuccess(returnCode)) {
      //     print('ob1111111111111ject');
      //     print(value);
      imageFile = XFile('${value['data'][0].path}');
      _startVideoPlayer();

      //     // SUCCESS
      //   } else if (ReturnCode.isCancel(returnCode)) {
      //     // CANCEL
      //     print('ob222222222222222222222ject');
      //   } else {
      //     // ERROR
      //     print('ob23333333333333333ject');
      //   }
      // });
    });
  }

  void saveCanvasToImage() async {
    // 获取RenderRepaintBoundary
    RenderRepaintBoundary boundary =
        globalKey.currentContext!.findRenderObject() as RenderRepaintBoundary;
    // 将渲染内容转换为图片
    final image = await boundary.toImage();
    print(image);

    // 将图片转换为ByteData
    final byteData = await image.toByteData(format: ImageByteFormat.png);
    // 将ByteData转换为Uint8List
    final pngBytes = byteData!.buffer.asUint8List();
    print(pngBytes);

    // 将图片保存到相册
    final result = await ImageGallerySaver.saveImage(pngBytes);
    print('object');
    print(result['filePath']);
    videoWatermarkModel(result['filePath']);
  }

  void videoWatermarkModel(path) {
    print('522323232');
    VideoWatermark(
      sourceVideoPath: '/storage/emulated/0/Movies/VID_20240528_020936.mp4',
      watermark: Watermark(image: WatermarkSource.file(File(path) as String)),
      onSave: (res) {
        // Get output file path
        print(res);
      },
      progress: (value) {
        // Get video generation progress
        print(value);
      },
    );
  }

  /// 打水印

  @override
  void dispose() {
    ///
    _controller.dispose();
    _videoController?.dispose();
    _time?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (!_isInitialized) {
      return Container();
    }
    // 获取屏幕高度
    final size = MediaQuery.of(context).size;
    // 获取状态栏高度
    final statusHeight = MediaQuery.of(context).padding.top;

    return Scaffold(
      // appBar: AppBar(
      //   title: const Text('Camera example'),
      // ),
      body: SizedBox(
        height: size.height,
        child: Stack(
          children: [
            SizedBox(
              width: double.infinity,
              height: size.height * 0.8,
              child: CameraPreview(_controller,
                  child: GestureDetector(
                    child: Container(
                      alignment: Alignment.bottomLeft,
                      child: RepaintBoundary(
                        key: globalKey,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text('示例文字'),
                            Image.asset(
                              'assets/img/login/systemLogo.png',
                              width: 20,
                              height: 20,
                            ),
                          ],
                        ),
                      ),
                    ),
                    onTap: () {
                      print('水印图');
                      saveCanvasToImage();
                    },
                  )),
            ),
            // 取消
            Positioned(
                top: statusHeight,
                left: Style.paddingThree,
                child: IconButton(
                    onPressed: () {
                      Get.back();
                    },
                    icon: Icon(
                      ModelIcon.clear,
                      color: Style.white,
                    ))),
            // 转摄像头拍照
            Obx(
              () => Positioned(
                  bottom: size.height * 0.2,
                  right: Style.paddingThree,
                  child: !_onFileRmine.value
                      ? _count <= 1
                          ? const Text('')
                          : Text(
                              '${'cameraCount'.tr}：$_count S',
                              style: TextStyle(
                                  color: Style.red,
                                  fontSize: Style.fontSizeSixteen),
                            )
                      : const Text('')),
            ),
            // 按钮
            Positioned(
                bottom: Style.padding,
                left: 0,
                right: 0,
                child: SizedBox(
                  width: double.infinity,
                  height: size.height * 0.2 - Style.padding,
                  child: Row(
                    children: [
                      Obx(() => Expanded(
                          child: IconButton(
                              onPressed: _switchingFlash,
                              icon: Icon(_flashIcon())))),
                      // flex: 2,
                      GestureDetector(
                        onTap: _takePicture,
                        onLongPress: _takePictureVideo,
                        child: Container(
                          width: size.height * 0.2 -
                              Style.padding -
                              Style.padding -
                              Style.paddingSix,
                          height: size.height * 0.2 -
                              Style.padding -
                              Style.padding -
                              Style.paddingSix,
                          decoration: BoxDecoration(
                              color: Style.green,
                              borderRadius: BorderRadius.circular(
                                  Style.borderRadiusForty)),
                          child: Icon(
                            ModelIcon.cameraAlt,
                            color: Style.white,
                          ),
                        ),
                      ),
                      Expanded(
                          child: IconButton(
                              onPressed: _switchingCameraVideo,
                              icon: Icon(ModelIcon.autorenew))),
                      Expanded(
                          child: IconButton(
                              onPressed: _switchingCamera,
                              icon: Icon(ModelIcon.autorenew))),
                    ],
                  ),
                )),
          ],
        ),
      ),
    );
  }
}
