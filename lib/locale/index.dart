import '/locale/zh_cn.dart';
import '/locale/en_us.dart';

Map<String,Map<String,String>> index = {
  'en_US': enUs,
  'zh_CN': zhCn,
};