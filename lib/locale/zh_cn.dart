import './permission/permission.dart'; // 隐私协议
import './login/login.dart'; // 登录
import './perfect/perfect.dart'; // 完善信息
import './appbar/appbar.dart'; // tab问题
import './appbar/home.dart'; // 首页

Map<String, String> zhCn = {
  'name': '爱工匠',
  'confirm': '确认',
  'cancel': '取消',
  'title': '系统提示',
  'titleMessage': '登录信息已过期请重新登录',
  'enter': '请输入',
  'select': '请选择',
  'successful': '操作成功！',
  'errorful': '操作失败！',
  'more': '更多',
  ...permission,
  ...login,
  ...perfect,
  ...appbar,
  ...home,
};
