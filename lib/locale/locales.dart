import 'package:get/get.dart';
import '/locale/index.dart';

class Locales extends Translations {
  @override
  Map<String, Map<String, String>> get keys => index;
}
