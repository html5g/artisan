Map<String, String> home = {
  'homeProject': '工程项目',
  'homeEpoxy': '环氧项目',
  'homeData': '数据统计',
  'homeBoard': '人员看板',
  'homeShare': '环氧份额',
  'homeRecruit': '招工报名',
  'homeMaterial': '工业材料',
  'homeFactory': '智慧工厂',
  'homeApprove': '审批',
  'homeReport': '工作汇报',
  'homeStudy': '学习考试',
  'homeCamera': '工匠相机',
};
