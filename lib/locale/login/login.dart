Map<String, String> login = {
  'loginAgree': '我已阅读并同意',
  'loginAgreement': '用户服务协议',
  'loginPrivacy': '隐私协议',
  'loginCraftsman': '未注册的手机号将自动创建',
  'loginAccount': '账号',
  'loginLogon': '登录',
  'loginPhone': '手机号',
  'loginPassword': '密码',
  'loginCode': '验证码',
  'loginObtain': '获取验证码',
  'agreewith': '同意',
  'disagree': '不同意',
};
