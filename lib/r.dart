class R {
  /// ![](http://127.0.0.1:9527/assets/img/middle/wait.png)
  static const String assetsImgMiddleWait = 'assets/img/middle/wait.png';

  /// ![](http://127.0.0.1:9527/assets/img/login/systemLogo.png)
  static const String assetsImgLoginSystemLogo =
      'assets/img/login/systemLogo.png';

  /// ![](http://127.0.0.1:9527/assets/img/login/whiteLogo.png)
  static const String assetsImgLoginWhiteLogo =
      'assets/img/login/whiteLogo.png';

  /// ![](http://127.0.0.1:9527/assets/img/index/approve.png)
  static const String assetsImgIndexApprove = 'assets/img/index/approve.png';

  /// ![](http://127.0.0.1:9527/assets/img/index/banner.jpg)
  static const String assetsImgIndexBanner = 'assets/img/index/banner.jpg';

  /// ![](http://127.0.0.1:9527/assets/img/index/bannerOne.png)
  static const String assetsImgIndexBannerOne =
      'assets/img/index/bannerOne.png';

  /// ![](http://127.0.0.1:9527/assets/img/index/bannerThree.png)
  static const String assetsImgIndexBannerThree =
      'assets/img/index/bannerThree.png';

  /// ![](http://127.0.0.1:9527/assets/img/index/bannerTwo.png)
  static const String assetsImgIndexBannerTwo =
      'assets/img/index/bannerTwo.png';

  /// ![](http://127.0.0.1:9527/assets/img/index/board.png)
  static const String assetsImgIndexBoard = 'assets/img/index/board.png';

  /// ![](http://127.0.0.1:9527/assets/img/index/data.png)
  static const String assetsImgIndexData = 'assets/img/index/data.png';

  /// ![](http://127.0.0.1:9527/assets/img/index/epoxy.png)
  static const String assetsImgIndexEpoxy = 'assets/img/index/epoxy.png';

  /// ![](http://127.0.0.1:9527/assets/img/index/factory.png)
  static const String assetsImgIndexFactory = 'assets/img/index/factory.png';

  /// ![](http://127.0.0.1:9527/assets/img/index/material.png)
  static const String assetsImgIndexMaterial = 'assets/img/index/material.png';

  /// ![](http://127.0.0.1:9527/assets/img/index/project.png)
  static const String assetsImgIndexProject = 'assets/img/index/project.png';

  /// ![](http://127.0.0.1:9527/assets/img/index/recruit.png)
  static const String assetsImgIndexRecruit = 'assets/img/index/recruit.png';

  /// ![](http://127.0.0.1:9527/assets/img/index/report.png)
  static const String assetsImgIndexReport = 'assets/img/index/report.png';

  /// ![](http://127.0.0.1:9527/assets/img/index/share.png)
  static const String assetsImgIndexShare = 'assets/img/index/share.png';

  /// ![](http://127.0.0.1:9527/assets/img/index/study.png)
  static const String assetsImgIndexStudy = 'assets/img/index/study.png';
}
