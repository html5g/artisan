import 'package:flutter/material.dart';
import 'package:get/get.dart';

/// 自定义导航
class UrAppbar extends StatelessWidget implements PreferredSizeWidget {
  /// 标题
  final String title;

  /// 是否自定义title
  final Widget? titleWidget;

  /// 左边ICon图标
  final IconData iconleading;

  /// title是否居中
  final bool centerTitle;
  // 自定义左边icon Widget
  final Widget? leading;

  const UrAppbar({
    super.key,
    this.title = '',
    this.titleWidget,
    this.centerTitle = true,
    this.iconleading = Icons.chevron_left_rounded,
    this.leading,
  });

  @override
  Widget build(BuildContext context) {
    return AppBar(
        centerTitle: centerTitle,
        titleSpacing: 0,
        title: titleWidget ?? Text(title),
        leading: leading ??
            IconButton(
              padding: const EdgeInsets.all(0),
              icon: Icon(iconleading),
              onPressed: () {
                Get.back();
              },
            ));
  }

  @override
  Size get preferredSize => const Size.fromHeight(kToolbarHeight);
}
