import 'package:flutter/material.dart';
// import 'package:flutter_easyrefresh/easy_refresh.dart';
import 'package:easy_refresh/easy_refresh.dart';

/// 下拉刷新组件
class UrRefresh extends StatelessWidget {
  /// 下拉刷新回调
  final Function? onRefresh;

  /// 上拉加载回调
  final Function? onLoad;

  /// 自定义下拉刷新组件
  final Header? header;

  /// 自定义上拉加载组件
  final Footer? footer;

  /// 是否展示骨架
  final bool sample;

  /// 组件盒子
  final Widget? child;
  const UrRefresh(
      {super.key,
      this.child,
      this.onRefresh,
      this.onLoad,
      this.header,
      this.footer,
      this.sample = true});

  @override
  Widget build(BuildContext context) {
    return EasyRefresh(
      triggerAxis: Axis.vertical,
      header: header ?? const DeliveryHeader(),
      footer: footer ?? const DeliveryFooter(),
      onRefresh: () => onRefresh!(),
      onLoad: () => onLoad!(),
      child: child,
    );
  }
}
