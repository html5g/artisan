import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import '/style/style.dart';

class UrField extends StatelessWidget {
  final String type;

  /// 右边文案
  final String? title;

  /// 默认输入框类容
  final String? initialValue;

  /// 输入框提示文案
  final String? hintText;

  /// 输入框对齐方式
  final TextAlign textAlign;

  /// 输入框占比
  final int flex;

  /// 输入框背景
  final Color? fillColor;

  /// 输入框圆角
  final double? borderRadius;

  /// 是否隐藏文本
  final bool obscureText;

  /// 禁止输入
  final bool enabled;

  /// 右边widget
  final Widget? suffixIcon;
  final Widget? childWidget;

  /// 验证函数
  final Function? onSaved;

  /// 验证函数
  final Function? validator;

  /// 输入框改变回调函数
  final Function? onChanged;

  /// 输入框函数
  final Function? onTap;
  const UrField(
      {super.key,
      this.type = 'text',
      this.title,
      this.initialValue,
      this.hintText,
      this.textAlign = TextAlign.right,
      this.flex = 3,
      this.fillColor,
      this.borderRadius,
      this.obscureText = false,
      this.enabled = true,
      this.suffixIcon,
      this.childWidget,
      this.onSaved,
      this.validator,
      this.onChanged,
      this.onTap});

  /// 文本输入框
  Widget _urTextField(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        Expanded(
            child: Row(
          children: [
            Expanded(
              flex: 1,
              child: Text(
                title ?? '',
                maxLines: 2,
                style: TextStyle(
                  fontSize: Style.fontSizeFifteen.sp,
                ),
                overflow: TextOverflow.ellipsis,
              ),
            ),
          ],
        )),
        SizedBox(
          width: Style.padding,
        ),
        Expanded(
            flex: flex,
            child: childWidget ??
                TextField(
                  enabled: enabled,
                  style: TextStyle(
                    fontSize: Style.fontSizeSixteen.sp,
                  ),
                  controller: TextEditingController(text: initialValue),
                  textAlign: textAlign,
                  obscureText: obscureText,
                  decoration: InputDecoration(
                    suffixIcon: suffixIcon,
                    hintText: hintText,
                    border: InputBorder.none,
                    contentPadding: EdgeInsets.fromLTRB(
                        Style.paddingThree, 0, Style.paddingThree, 0),
                  ),
                  onChanged: (value) => onChanged!(value),
                )),
      ],
    );
  }

  Widget _urFieldView(BuildContext context) {
    switch (type) {
      case 'text':
        return _urTextField(context);
      default:
        return _urTextField(context);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          border:
              Border(bottom: BorderSide(color: Style.lightGray, width: .1))),
      child: _urFieldView(context),
    );
  }
}
