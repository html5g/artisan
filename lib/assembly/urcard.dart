import 'package:flutter/material.dart';
import '/style/style.dart';

class UrCard extends StatelessWidget {
  final Widget? child;
  const UrCard({super.key, this.child});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      margin: EdgeInsets.fromLTRB(
          Style.padding, Style.paddingSix, Style.padding, Style.paddingSix),
      padding: EdgeInsets.all(Style.padding),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(Style.borderRadiusFive),
        color: Style.white,
      ),
      child: child,
    );
  }
}
