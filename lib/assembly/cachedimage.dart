import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';

class CachedImage extends StatelessWidget {
  final String imageUrl; // 图片地址
  final double? width; // 宽度
  final double? height; // 高度
  final BoxFit? fit; // 切割方式
  final Alignment alignment; // 对齐方式

  const CachedImage(
      {super.key,
      required this.imageUrl,
      this.width,
      this.height,
      this.fit,
      this.alignment = Alignment.center});

  // 判断是否含有
  Widget cachedImg() {
    String pattern = r'^https?://[^\\s]*$'; // 匹配http或https开头，后面跟着非空白字符的字符串
    RegExp regex = RegExp(pattern);
    return regex.hasMatch(imageUrl)
        ? CachedNetworkImage(
            imageUrl: imageUrl,
            alignment: alignment,
            width: width,
            height: height,
            placeholder: (context, url) => const CircularProgressIndicator(),
            errorWidget: (context, url, error) => const Icon(Icons.error),
          )
        : Image.asset(
            imageUrl,
            width: width,
            height: height,
            fit: fit,
            alignment: alignment,
            errorBuilder: (context, url, error) => const Icon(Icons.error),
          );
  }

  @override
  Widget build(BuildContext context) {
    return cachedImg();
  }
}
