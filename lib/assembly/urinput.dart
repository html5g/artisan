import 'package:flutter/material.dart';
import '/style/style.dart';

/// 自定义输入框表单认证
class UrInput extends StatelessWidget {
  /// 默认输入框类容
  final String? initialValue;

  /// 输入框提示文案
  final String? hintText;

  /// 输入框对齐方式
  final TextAlign textAlign;

  /// 输入框背景
  final Color? fillColor;

  /// 输入框圆角
  final double? borderRadius;

  /// 是否隐藏文本
  final bool obscureText;

  /// 右边widget
  final Widget? suffixIcon;

  /// 验证函数
  final Function? onSaved;

  /// 验证函数
  final Function? validator;

  /// 输入框函数
  final Function? onChanged;
  const UrInput({
    super.key,
    this.initialValue,
    this.hintText,
    this.textAlign = TextAlign.left,
    this.fillColor,
    this.borderRadius,
    this.obscureText = false,
    this.suffixIcon,
    this.onSaved,
    this.validator,
    this.onChanged,
  });

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      textAlign: textAlign,
      initialValue: initialValue,
      obscureText: obscureText,
      decoration: InputDecoration(
        suffixIcon: suffixIcon,
        hintText: hintText,
        // errorStyle: const TextStyle(fontSize: 0),
        enabledBorder: UnderlineInputBorder(
          //没有焦点时
          borderSide: BorderSide.none,
          borderRadius:
              BorderRadius.circular(borderRadius ?? Style.borderRadiusForty),
        ),
        focusedBorder: UnderlineInputBorder(
          //有焦点时
          borderSide: BorderSide.none,
          borderRadius:
              BorderRadius.circular(borderRadius ?? Style.borderRadiusForty),
        ),
        border: OutlineInputBorder(
          borderSide: BorderSide.none,
          borderRadius:
              BorderRadius.circular(borderRadius ?? Style.borderRadiusForty),
        ),
        filled: true,
        fillColor: fillColor ?? Style.backGrey,
        contentPadding: const EdgeInsets.fromLTRB(20.0, 0.0, 20.0, 0.0),
      ),
      validator: (value) => validator!(value),
      onSaved: (value) => onSaved!(value),
      onChanged: (value) => onChanged!(value),
    );
  }
}
