import 'package:flutter/material.dart';
import '/style/style.dart';

class UrLabel extends StatelessWidget {
  final String title;
  final Widget child;
  const UrLabel({super.key, required this.title, required this.child});

  @override
  Widget build(BuildContext context) {
    throw ListTile(title: Text(title), trailing: child);
  }
}
