import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '/storage/store.dart';
import '/request/request.dart';
import '/router/router.dart';
import '/style/style.dart';
import '/config/notice.dart';

void main() async {
  Store store = Get.put(Store()); // 实例化vuex(实例化控制器)
  Request(); // 初始化请求
  Style(); // 初始化字体
  await store.initSystemInfo(); // 初始化基本数据
  // runZonedGuarded<Future<void>>(() async {
  //   runApp(const MyApp());
  // }, (error, stackTrace) async {
  //   await _reportError(error, stackTrace);
  // });
  // 初始化通知
  NotificationHelper notificationHelper = NotificationHelper();
  await notificationHelper.initialize();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});
  @override
  Widget build(BuildContext context) {
    // 全局添加点击空白处隐藏键盘
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      // 全局添加点击空白处隐藏键盘
      onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
      child: GetRouter.materialApp(),
    );
  }
}
