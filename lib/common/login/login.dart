// 登录相关接口

import '/request/request.dart';

/// 登录接口
///
/// 时间：2024/5/8 An
///* 接口：authApi/auth/GetAppAuthCode
///* 入参：phone(手机号) source(int 来源 1 An卓 2 苹果)  appId(应用id) password(密码) code(验证码) phoneCode(获取微信手机号) loginType(手机号登陆 0 密码登录 1 授权登陆_微信  2 授权登陆_WX手机号 3  三方授权登陆App 4  三方授权一键登陆App 5)
login({data}) =>
    Request.post(method: '/authApi/auth/GetAppAuthCode', data: data);

/// 获取用户消息
///
/// 时间：2024/5/8 An
///* 接口：/authApi/auth/GetUserInfo
///* 入参：appId(应用id)
userInfo({data}) =>
    Request.post(method: '/authApi/auth/GetUserInfo', data: data);

/// 获取验证码
///
/// 时间：2024/5/8 An
///* 接口：/authApi/auth/GetVerificationCode
///* 入参：appId(应用id) phone(手机号)
userCationCode({data}) =>
    Request.get(method: '/authApi/auth/GetVerificationCode', data: data);

/// 获取认证企业
///
/// 时间：2024/5/11 An
///* 接口：/authApi/EppSysCorp/GetEppSysCorpList
///* 入参：appId(应用id) PageNo(页码) PageSize（条码）SearchValue（搜索条件）
userCorp({data}) =>
    Request.post(method: '/authApi/EppSysCorp/GetEppSysCorpList', data: data);

/// 注册用户
///
/// 时间：2024/5/11 An
///* 接口：/authApi/auth/RegisterByApp
///* 入参：appId(应用id) name(名称) phone（手机号）invitationCode（邀请码） createSource (创建来源 1 推广注册 2 企业引荐/注册)
userRegister({data}) =>
    Request.post(method: '/authApi/auth/RegisterByApp', data: data);

/// 加入企业
///
/// 时间：2024/5/11 An
///* 接口：/authApi/auth/RegisterByApp
///* 入参：userId(应用id) account(爱工匠账号) name（姓名）corpName（企业名称） corpId 企业名称
userAddCorp({data}) =>
    Request.post(method: '/authApi/CorpUser/JoinCorp', data: data);
