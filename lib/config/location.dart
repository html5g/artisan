import 'package:geolocator/geolocator.dart';
import '/request/request.dart';

class Location {
  // 获取定位信息
  static Future<Map> getCurrentLocation() async {
    try {
      bool isLocationServiceEnabled =
          await Geolocator.isLocationServiceEnabled();
      // 是否开启了定位服务
      if (isLocationServiceEnabled) {
        Position position = await Geolocator.getCurrentPosition(
            desiredAccuracy: LocationAccuracy.high);
        var data = await _geoCoder(position: position);
        return Future.value(data);
      }
      return Future.error({'code': 500});
    } catch (e) {
      return Future.error(e);
    }
  }

  // 获取定位信息将经纬度转换具体信息
  static Future<Map> _geoCoder({position}) async {
    Map positionInfo = await Request.post(
            method: '/api/PublicMethod/GeoCoder',
            data: {'Lng': position.longitude, 'Lat': position.latitude})
        .then((res) {
      return Future.value({
        'latitude': position.latitude,
        'longitude': position.longitude,
        'address': res['data']['result']['formatted_Address']
      });
    }).catchError((err) {
      return Future.value({
        'latitude': position.latitude,
        'longitude': position.longitude,
        'address': ''
      });
    });
    return positionInfo;
  }
}
