import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:get/get.dart';
import '/config/notice.dart';
import '/config/location.dart';
import '/config/imagecamera.dart';
import '/storage/store.dart';
import '/allocation/allocation.dart';
import '/style/style.dart';
import 'dart:io';

class PermissionHandler {
  // 获取全局弹出框对象
  static BuildContext ctx = navigatorKey.currentState!.overlay!.context;
  // 获取权限记录
  static Store store = Get.put(Store());
  // 获取通知权限
  static Future<Map> requestNotification(
      {required String noticeTitle, required String noticeContent}) async {
    String showModal = ''; // 是1否0 s 受权
    // 第一次获取通知权限，提示获取权限的目的
    if (store.permission['notification'] == null) {
      showModal = await _showModalBottomSheet(
          title: 'noticeTitle'.tr, content: 'noticeMsg'.tr);
    }
    if (showModal == '0') {
      var showSheet = await _showModalCenterSheet(); // 是1否0  受权
      // 没有受权
      if (showSheet == '0') {
        return Future.error({'code': 500, 'statusCode': 'denied'});
      }
    }
    PermissionStatus status = await Permission.notification.request();
    // // 检查用户是否授权
    // PermissionStatus? status = permissions[Permission.notification];

    // permanentlyDenied 永久拒绝 denied 否认 granted 同意
    //  limited 有限的 provisional 临时的 restricted 有限的
    // other 自定义其他
    if (status == PermissionStatus.granted) {
      await NotificationHelper.showNotification(
          noticeTitle: noticeTitle, noticeContent: noticeContent);
      store.permission['notification'] = 'granted';
      store.setPermission();
      return Future.value({'code': 200, 'statusCode': 'granted'});
    }
    if (status == PermissionStatus.permanentlyDenied) {
      store.permission['notification'] = 'permanentlyDenied';
      store.setPermission();
      var showSheet =
          await _showModalCenterSheet(content: 'noticePermanently'.tr);
      // 跳转到系统详情设置页面 // 是1否0  受权
      if (showSheet == '1') openAppSettings();
      return Future.error({'code': 500, 'statusCode': 'permanentlyDenied'});
    }
    if (status == PermissionStatus.denied) {
      store.permission['notification'] = 'denied';
      // await requestNotification();
      store.setPermission();
      var showSheet = await _showModalCenterSheet(content: 'noticeDenied'.tr);
      // 跳转到系统详情设置页面 // 是1否0  受权
      if (showSheet == '1') {
        await requestNotification(
            noticeTitle: noticeTitle, noticeContent: noticeContent);
      }
      return Future.error({'code': 500, 'statusCode': 'denied'});
    }
    store.permission['notification'] = 'other';
    store.setPermission();
    return Future.error({'code': 500, 'statusCode': 'other'});
  }

  // 获取定位权限
  static Future<Map> requestLocation() async {
    String showModal = ''; // 是1否0 s 受权
    // 第一次获取通知权限，提示获取权限的目的
    if (store.permission['location'] == null) {
      showModal = await _showModalBottomSheet(
          title: 'locationTitle'.tr, content: 'locationMsg'.tr);
    }
    if (showModal == '0') {
      var showSheet = await _showModalCenterSheet(); // 是1否0  受权
      // 没有受权
      if (showSheet == '0') {
        return Future.error({'code': 500, 'statusCode': 'denied'});
      }
    }
    PermissionStatus status = await Permission.location.request();
    // permanentlyDenied 永久拒绝 denied 否认 granted 同意
    //  limited 有限的 provisional 临时的 restricted 有限的
    // other 自定义其他
    if (status == PermissionStatus.granted) {
      store.permission['location'] = 'granted';
      store.setPermission();
      // ignore: body_might_complete_normally_catch_error
      var location = await Location.getCurrentLocation().catchError((err) {
        // 没有开启定位
        _showModalCenterSheet(content: 'locationServer'.tr);
      });
      return Future.value(
          {'code': 200, 'statusCode': 'granted', 'data': location});
    }
    if (status == PermissionStatus.permanentlyDenied) {
      store.permission['location'] = 'permanentlyDenied';
      store.setPermission();
      var showSheet =
          await _showModalCenterSheet(content: 'locationPermanently'.tr);
      // 跳转到系统详情设置页面 // 是1否0  受权
      if (showSheet == '1') openAppSettings();
      return Future.error(
          {'code': 500, 'statusCode': 'permanentlyDenied', 'data': null});
    }
    if (status == PermissionStatus.denied) {
      store.permission['location'] = 'denied';
      store.setPermission();
      var showSheet = await _showModalCenterSheet(content: 'locationDenied'.tr);
      // 跳转到系统详情设置页面 // 是1否0  受权
      if (showSheet == '1') {
        await requestLocation();
      }
      return Future.error({'code': 500, 'statusCode': 'denied', 'data': null});
    }
    store.permission['location'] = 'other';
    store.setPermission();
    return Future.error({'code': 500, 'statusCode': 'other', 'data': null});
  }

  // 获取摄像头权限
  static Future<Map> requestCamera() async {
    String showModal = ''; // 是1否0 s 受权
    // 第一次获取通知权限，提示获取权限的目的
    if (store.permission['camera'] == null) {
      showModal = await _showModalBottomSheet(
          title: 'cameraTitle'.tr, content: 'cameraMsg'.tr);
    }
    if (showModal == '0') {
      var showSheet = await _showModalCenterSheet(); // 是1否0  受权
      // 没有受权
      if (showSheet == '0') {
        return Future.error({'code': 500, 'statusCode': 'denied'});
      }
    }
    PermissionStatus status = await Permission.camera.request();
    // permanentlyDenied 永久拒绝 denied 否认 granted 同意
    //  limited 有限的 provisional 临时的 restricted 有限的
    // other 自定义其他
    if (status == PermissionStatus.granted) {
      store.permission['camera'] = 'granted';
      store.setPermission();
      var camera = await ImageCamera.getCamera();
      return Future.value(
          {'code': 200, 'statusCode': 'granted', 'data': camera});
    }
    if (status == PermissionStatus.permanentlyDenied) {
      store.permission['camera'] = 'permanentlyDenied';
      store.setPermission();
      var showSheet =
          await _showModalCenterSheet(content: 'cameraPermanently'.tr);
      // 跳转到系统详情设置页面 // 是1否0  受权
      if (showSheet == '1') openAppSettings();
      return Future.error(
          {'code': 500, 'statusCode': 'permanentlyDenied', 'data': null});
    }
    if (status == PermissionStatus.denied) {
      store.permission['camera'] = 'denied';
      store.setPermission();
      var showSheet = await _showModalCenterSheet(content: 'cameraDenied'.tr);
      // 跳转到系统详情设置页面 // 是1否0  受权
      if (showSheet == '1') {
        await requestCamera();
      }
      return Future.error({'code': 500, 'statusCode': 'denied', 'data': null});
    }
    store.permission['camera'] = 'other';
    store.setPermission();
    return Future.error({'code': 500, 'statusCode': 'other', 'data': null});
  }

  // 获取相册权限
  static Future<Map> requestAlbum() async {
    String showModal = ''; // 是1否0 s 受权
    // 第一次获取通知权限，提示获取权限的目的
    if (store.permission['album'] == null) {
      showModal = await _showModalBottomSheet(
          title: 'albumTitle'.tr, content: 'albumMsg'.tr);
    }
    if (showModal == '0') {
      var showSheet = await _showModalCenterSheet(); // 是1否0  受权
      // 没有受权
      if (showSheet == '0') {
        return Future.error({'code': 500, 'statusCode': 'denied'});
      }
    }
    // 安卓12以下使用文件管理器选择图片
    PermissionStatus status;
    if (Platform.isAndroid) {
      int androidVersion = 12;
      List<String> versionComponents =
          Platform.operatingSystemVersion.split(' ');
      if (versionComponents.length > 1) {
        try {
          androidVersion = int.parse(versionComponents[1]);
        } catch (e) {
          androidVersion = 12;
        }
      } else {
        androidVersion = 12;
      }
      if (androidVersion <= 12) {
        status = await Permission.storage.request();
      } else {
        status = await Permission.photos.request();
      }
    } else {
      status = await Permission.photos.request();
    }
    // permanentlyDenied 永久拒绝 denied 否认 granted 同意
    //  limited 有限的 provisional 临时的 restricted 有限的
    // other 自定义其他
    if (status == PermissionStatus.granted) {
      store.permission['album'] = 'granted';
      store.setPermission();
      var album = await ImageCamera.getMultiple();
      return Future.value(
          {'code': 200, 'statusCode': 'granted', 'data': album});
    }
    if (status == PermissionStatus.permanentlyDenied) {
      store.permission['album'] = 'permanentlyDenied';
      store.setPermission();
      var showSheet =
          await _showModalCenterSheet(content: 'albumPermanently'.tr);
      // 跳转到系统详情设置页面 // 是1否0  受权
      if (showSheet == '1') openAppSettings();
      return Future.error(
          {'code': 500, 'statusCode': 'permanentlyDenied', 'data': null});
    }
    if (status == PermissionStatus.denied) {
      store.permission['album'] = 'denied';
      store.setPermission();
      var showSheet = await _showModalCenterSheet(content: 'albumDenied'.tr);
      // 跳转到系统详情设置页面 // 是1否0  受权
      if (showSheet == '1') {
        await requestAlbum();
      }
      return Future.error({'code': 500, 'statusCode': 'denied', 'data': null});
    }
    store.permission['album'] = 'other';
    store.setPermission();
    return Future.error({'code': 500, 'statusCode': 'other', 'data': null});
  }

  // 获取第一次权限公共弹出框
  static Future<String> _showModalBottomSheet(
      {String? title,
      String? content,
      String? confirmText,
      String? canceText}) async {
    title ??= '${'name'.tr}${'privacy'.tr}';
    content ??= '${'name'.tr}${'privacy'.tr}';
    confirmText ??= "confirm".tr;
    canceText ??= "cancel".tr;
    var showModal = await showModalBottomSheet(
      context: ctx,
      isDismissible: false,
      builder: (BuildContext context) {
        return Container(
            padding: EdgeInsets.all(Style.padding),
            decoration: BoxDecoration(
                color: Style.white,
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(Style.borderRadiusTwelve),
                    topRight: Radius.circular(Style.borderRadiusTwelve))),
            child: Stack(
              children: [
                ListView(
                  // mainAxisSize: MainAxisSize.min,
                  children: [
                    const SizedBox(
                      height: 40,
                    ),
                    RichText(
                      text: TextSpan(
                        style: TextStyle(
                            color: Style.black,
                            fontSize: Style.fontSizeFifteen.sp),
                        text: content,
                      ),
                    ),
                    const SizedBox(height: 90.0),
                  ],
                ),
                // 顶部标题文案
                Positioned(
                    top: -2.0,
                    left: 0.0,
                    right: 0.0,
                    child: Container(
                      width: double.infinity,
                      alignment: Alignment.center,
                      color: Style.white,
                      // padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
                      child: Text(
                        '$title',
                        style: TextStyle(
                          fontSize: Style.fontSizeEighteen.sp,
                        ),
                      ),
                    )),
                // 底部确认和取消按钮
                Positioned(
                    bottom: 0,
                    left: 0.0,
                    right: 0.0,
                    child: Container(
                      color: Style.white,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Expanded(
                              flex: 1,
                              child: TextButton(
                                  onPressed: () {
                                    Get.back(result: '0');
                                  },
                                  child: Text("cancel".tr))),
                          Expanded(
                              flex: 2,
                              child: TextButton(
                                  style: ButtonStyle(
                                      backgroundColor:
                                          MaterialStateProperty.all(
                                              Style.green)),
                                  onPressed: () {
                                    Get.back(result: '1');
                                  },
                                  child: Text(
                                    "confirm".tr,
                                    style: TextStyle(color: Style.white),
                                  )))
                        ],
                      ),
                    ))
              ],
            ));
      },
    );
    return showModal;
  }

  // 拒绝权限公共弹出框
  static Future<String> _showModalCenterSheet(
      {String? title,
      String? content,
      String? confirmText,
      String? canceText}) async {
    title ??= "title".tr;
    content ??= "content".tr;
    confirmText ??= "confirm".tr;
    canceText ??= "cancel".tr;
    var showModal = await showCupertinoDialog(
      context: ctx,
      builder: (context) {
        return CupertinoAlertDialog(
          title: Text('$title'),
          content: Text('$content'),
          actions: [
            CupertinoDialogAction(
              child: Text('$canceText', style: TextStyle(color: Style.red)),
              onPressed: () {
                Get.back(result: '0');
              },
            ),
            CupertinoDialogAction(
              child: Text('$confirmText', style: TextStyle(color: Style.green)),
              onPressed: () {
                Get.back(result: '1');
              },
            ),
          ],
        );
      },
    );
    return showModal;
  }
}
