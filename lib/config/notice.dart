import 'package:flutter_local_notifications/flutter_local_notifications.dart';

class NotificationHelper {
  // 使用单例模式进行初始化
  // static final NotificationHelper _instance = NotificationHelper._internal();
  // factory NotificationHelper() => _instance;
  // NotificationHelper._internal();
  static const String darwinNotificationCategoryPlain = 'plainCategory';
  static int noticeId = 1; // 推送ID ，不同的值可以展示多个通知信息
  // static const BigTextStyleInformation bigTextStyleInformation =
  //     BigTextStyleInformation('dasdasdasdasdasdasdsaaaaaaaaaaaa');

  // 发送通知
  static final FlutterLocalNotificationsPlugin _notificationsPlugin =
      FlutterLocalNotificationsPlugin();
  // 显示一条消息推送
  static Future<void> showNotification(
      {required String noticeTitle, required String noticeContent}) async {
    // 这里可以使用第三方库如flutter_local_notifications来发送通知
    // 示例代码仅为说明，不包含实际的通知发送逻辑

    // flutter_local_notifications 插件使用方法请查看其文档
    const AndroidNotificationDetails androidNotificationDetails =
        AndroidNotificationDetails(
      'your.channel.id',
      'your channel name',
      channelDescription: 'your channel description',
      importance: Importance.max,
      priority: Priority.high,
      ticker: 'ticker',
      showWhen: true,
    );
    const DarwinNotificationDetails iosNotificationDetails =
        DarwinNotificationDetails(
      categoryIdentifier: darwinNotificationCategoryPlain, // 通知分类
    );
    // 创建跨平台通知
    const NotificationDetails platformChannelSpecifics = NotificationDetails(
        android: androidNotificationDetails, iOS: iosNotificationDetails);
    // 发起一个通知
    noticeId++;
    return await _notificationsPlugin.show(
      noticeId,
      noticeTitle,
      noticeContent,
      platformChannelSpecifics,
    );
  }

  // 初始化函数
  Future<void> initialize() async {
    // AndroidInitializationSettings是一个用于设置Android上的本地通知初始化的类
    // 使用了app_icon作为参数，这意味着在Android上，应用程序的图标将被用作本地通知的图标。
    const AndroidInitializationSettings initializationSettingsAndroid =
        AndroidInitializationSettings('@mipmap/ic_launcher');
    // 15.1是DarwinInitializationSettings，旧版本好像是IOSInitializationSettings（有些例子中就是这个）
    const DarwinInitializationSettings initializationSettingsIOS =
        DarwinInitializationSettings();
    // 初始化
    const InitializationSettings initializationSettings =
        InitializationSettings(
            android: initializationSettingsAndroid,
            iOS: initializationSettingsIOS);
    await _notificationsPlugin.initialize(initializationSettings);
  }
}
