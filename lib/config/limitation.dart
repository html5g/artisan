import 'package:easy_debounce/easy_throttle.dart';
import 'package:easy_debounce/easy_debounce.dart';

/// 防抖节流函数
///
///
class Limitation {
  static Duration defaultDuration({int time = 300}) {
    return Duration(milliseconds: time);
  }

  // 节流
  static debounce(
      {required String tag,
      required Duration duration,
      EasyDebounceCallback? onExecute}) {
    EasyDebounce.debounce(tag, duration, onExecute!);
  }

  // 防抖
  static throttle(
      {required tag,
      required duration,
      EasyDebounceCallback? onExecute,
      EasyDebounceCallback? onAfter}) {
    EasyThrottle.throttle(tag, duration, onExecute!, onAfter: onAfter);
  }
}
