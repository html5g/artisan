import 'package:image_picker/image_picker.dart';

/// 拍照或者选择视频和图片
///
class ImageCamera {
  // 获取摄像头实例
  static final ImagePicker _picker = ImagePicker();
  // 拍照
  static Future getCamera() async {
    final XFile? image = await _picker.pickImage(source: ImageSource.camera);
    return image;
  }

  // 选择图片和视频
  static Future getMultiple() async {
    final List<XFile> fileList = await _picker.pickMultipleMedia(limit: 2);
    return fileList;
  }
}
