import 'package:fluttertoast/fluttertoast.dart';
import '/style/style.dart';
import '/allocation/allocation.dart';
import 'package:flutter/material.dart';

class ToastInfo {
  // 获取全局弹出框对象
  static BuildContext ctx = navigatorKey.currentState!.overlay!.context;
  static final FToast fToast = fToast.init(ctx);
  static toast(
      {required String msg,
      double? fontSize,
      Color? textColor,
      Color? backgroundColor}) {
    Fluttertoast.showToast(
        msg: msg,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER,
        timeInSecForIosWeb: 1,
        backgroundColor: backgroundColor ?? Style.lightGray,
        textColor: textColor ?? Style.white,
        fontSize: fontSize ?? Style.fontSizeTwelve);
  }
}
