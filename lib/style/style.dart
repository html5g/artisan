import 'package:flutter/material.dart';

class Style {
  /// 初始化数据
  Style() {
    /// fontSizeTwenty = 30;
    /// middle = Colors.red;
  }

  /// 广告页跳过背景色
  static Color middle = const Color.fromRGBO(255, 255, 255, 0.7);

  /// 系统白色
  static Color white = const Color.fromRGBO(255, 255, 255, 1);

  /// 系统绿色
  static Color green = const Color.fromRGBO(8, 162, 131, 1);

  /// 系统绿色淡色
  static Color greenGradient = const Color.fromRGBO(8, 162, 131, .6);

  /// f8f8f8
  static Color backGrey = const Color.fromRGBO(248, 248, 248, 1);

  /// #999
  static Color lightGray = const Color.fromRGBO(153, 153, 153, 1);

  /// 系统红色
  static Color red = const Color.fromARGB(255, 255, 0, 0);

  /// 系统黑色
  static Color black = const Color.fromARGB(255, 0, 0, 0);

  /// 边框圆形20
  static BorderRadiusGeometry radiusTwenty = BorderRadius.circular(20);

  /// 边框圆形100
  static BorderRadiusGeometry radiusHundred = BorderRadius.circular(100);

  /// 字体20
  static double fontSizeTwenty = 20;

  /// 字体18
  static double fontSizeEighteen = 18;

  /// 字体16
  static double fontSizeSixteen = 16;

  /// 字体15
  static double fontSizeFifteen = 15;

  /// 字体12
  static double fontSizeTwelve = 12;

  /// 边框间距15
  static double padding = 15;

  /// 边框间距3
  static double paddingThree = 3;

  /// 边框间距6
  static double paddingSix = 6;

  /// 圆角5
  static double borderRadiusFive = 5;

  /// 圆角12
  static double borderRadiusTwelve = 12;

  /// 圆角40
  static double borderRadiusForty = 40;
}
