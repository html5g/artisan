//  环境声明
abstract class EnvState {
  //  环境key
  static const String envKey = "DART_DEFINE_APP_ENV";

  static const String debug = "debug";
  static const String test = "test";
  static const String pre = "pre";
  static const String release = "release";
}
// UNI_APP_BASE_API: "http://192.168.2.212:8019/", // 默认请求库
// // UNI_APP_BASE_API: "http://106.14.56.171:8019/", // 默认请求库
// UNI_APP_BASE_USER: "http://192.168.2.190:8032/", // 用户中兴请求库
// // UNI_APP_BASE_USER: "https://auth.epumping.com/test/", // 用户中兴请求库
// UNI_APP_BASE_PROC: "https://uni.epumping.com/proc/", //
// UNI_APP_AUTH_WAP: "2875c5c0-4824-4d01-bbde-c6bc3df2f40a", // wep 端appID
// UNI_APP_AUTH_AND: "2875c5c0-4824-4d01-bbde-c6bc3df2f40a", // 安卓 端appID
// UNI_APP_AUTH_APPID: "517f103f-eb19-41b2-9d23-60321aac098d", // 苹果 端appID
// UNI_APP_AUTH_WECHAT: "bc40e1c7-9165-42fa-b7c4-182308c5dfd4 ", // 微信小程序 端appID

class Env {
  //  获取当前环境
  static const appEnv = String.fromEnvironment(EnvState.envKey);

  static final _debugConfig = {
    'app_title': "开发",
    'app_base_api': "http://test_platformapi.epumping.com",
    'app_auth_green': '2875c5c0-4824-4d01-bbde-c6bc3df2f40a'
  };
  static final _testConfig = {
    'app_title': "测试",
    'app_base_api': "http://test_platformapi.epumping.com",
    'app_auth_green': '2875c5c0-4824-4d01-bbde-c6bc3df2f40a'
  };

  static final _preConfig = {
    'app_title': "灰度",
    'app_base_api': "http://test_platformapi.epumping.com",
    'app_auth_green': '2875c5c0-4824-4d01-bbde-c6bc3df2f40a'
  };

  static final _releaseConfig = {
    'app_title': "生产",
    'app_base_api': "http://test_platformapi.epumping.com",
    'app_auth_green': '2875c5c0-4824-4d01-bbde-c6bc3df2f40a'
  };

  static get envConfig => _getEnvConfig();

  static Map _getEnvConfig() {
    switch (appEnv) {
      case EnvState.debug:
        return _debugConfig;
      case EnvState.test:
        return _testConfig;
      case EnvState.pre:
        return _preConfig;
      case EnvState.release:
        return _releaseConfig;
      default:
        return _debugConfig;
    }
  }
}
