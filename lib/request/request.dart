import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart' as getx;
import '/request/network.dart';
import '/storage/store.dart';
import '/allocation/allocation.dart';
import '/config/toast.dart';

// 请求库
Map env = Env.envConfig;

class Request {
  // 创建请求实例
  static final Dio _dio = Dio();
  static Store store = getx.Get.put(Store());
  // 初始化
  Request() {
    // 添加请求前拦截器
    _dio.interceptors.add(InterceptorsWrapper(onRequest: (options, handler) {
      options.headers['Authorization'] = 'Bearer ${store.token}';
      return handler.next(options);
    }));
    // 添加请求后拦截器
    _dio.interceptors
        .add(InterceptorsWrapper(onResponse: (response, handler) async {
      return handler.next(response);
    }));
    // 添加错误拦截器
    _dio.interceptors.add(InterceptorsWrapper(onError: (e, handler) {
      return handler.next(e);
    }));
  }
  // 请求地址
  static String _netWorkUrl({baseUrl, method}) {
    return '${env['app_base_${baseUrl ?? 'api'}']}$method';
  }

  // 请求拦截
  static Map<String, dynamic> _beforeRequest({required data, convert = false}) {
    Map<String, dynamic> dataMap = {};
    if (data != null) {
      for (var entry in data.entries) {
        var entryValue = entry.value;
        // 是否转换字符串
        if (convert) {
          if (entry.value is List) {
            entryValue = json.encode(entry.value);
          } else if (entry.value is double) {
            entryValue = entry.value.toString();
          } else if (entry.value is int) {
            entryValue = entry.value.toString();
          } else if (entry.value is Map) {
            entryValue = jsonEncode(entry.value);
          } else {
            entryValue = entry.value;
          }
        }
        dataMap[entry.key] = entryValue;
      }
    }
    return dataMap;
  }

  // 响应拦截
  static Future _handleResponse(response) async {
    if (response.data['code'] == 401) {
      // 获取全局弹出框对象
      BuildContext ctx = navigatorKey.currentState!.overlay!.context;
      showCupertinoDialog(
        context: ctx,
        builder: (context) {
          return CupertinoAlertDialog(
            title: Text('title'.tr),
            content: Text('titleMessage'.tr),
            actions: [
              CupertinoDialogAction(
                child: Text('confirm'.tr),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      );
      return Future.error(response);
    }
    if (response.data['code'] == 200) {
      return Future.value(response.data);
    }
    // 提示信息
    ToastInfo.toast(msg: response.data['message'] ?? 'errorful'.tr);
    return Future.error(response.data);
  }

  // Get 请求
  static Future get(
      {required String method,
      String? baseUrl,
      Map<String, dynamic>? data}) async {
    try {
      Map<String, dynamic> before = _beforeRequest(data: data);
      String url = _netWorkUrl(baseUrl: baseUrl, method: method);
      Response response = await _dio.get(url, queryParameters: before);
      return await _handleResponse(response);
    } on DioException catch (e) {
      // ignore: use_rethrow_when_possible
      throw (e);
    }
  }

  // post 请求
  static Future post(
      {required String method,
      String? baseUrl,
      Map<String, dynamic>? data}) async {
    try {
      Map<String, dynamic> before = _beforeRequest(data: data);
      String url = _netWorkUrl(baseUrl: baseUrl, method: method);
      Response response = await _dio.post(url, data: before);
      return await _handleResponse(response);
    } on DioException catch (e) {
      // ignore: use_rethrow_when_possible
      throw (e);
    }
  }

  /// 文件上传方法
  /// @param {method}
  static Future uploadFile(
      {required String method,
      List<Map<String, String>>? file,
      String? baseUrl,
      Map<String, dynamic>? data,
      String fileName = 'file',
      void Function(int, int)? onSendProgress,
      void Function(int, int)? onReceiveProgress}) async {
    try {
      Map<String, dynamic> before = _beforeRequest(data: data, convert: true);
      FormData formData = FormData.fromMap({});
      // 其余数据
      for (var entry in before.entries) {
        formData.fields.add(MapEntry(entry.key, entry.value));
      }
      // 文件
      if (file != null) {
        for (var e in file) {
          formData.files.add(MapEntry(e['fileName'] ?? fileName,
              MultipartFile.fromFileSync(e['fileUrl'] ?? '')));
        }
      }
      String url = _netWorkUrl(baseUrl: baseUrl, method: method);
      Response response = await _dio.post(url,
          data: formData,
          onSendProgress: onSendProgress,
          onReceiveProgress: onReceiveProgress);
      return await _handleResponse(response);
    } on DioException catch (e) {
      // ignore: use_rethrow_when_possible
      throw (e);
    }
  }
}
