import 'package:artisan/style/style.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import '/locale/locales.dart';
import '/router/routes.dart';
import '/allocation/allocation.dart';
import '/storage/store.dart';
import '/allocation/routeserver.dart';

class GetRouter {
  // 名称
  static String title = 'name'.tr;
  // 动画
  static Transition animation = Transition.rightToLeft;
  // 默认路由
  static String initialRoute = '/';
  static Store store = Get.put(Store()); // 实例化vuex(实例化控制器)
  // Getx Widget
  static Widget materialApp() {
    return ScreenUtilInit(
      designSize: const Size(360, 640), // 设计稿中设备的尺寸(单位随意,建议dp,但在使用过程中必须保持一致)
      minTextAdapt: true, // 是否根据宽度/高度中的最小值适配文字
      splitScreenMode: true, // 支持分屏尺寸
      builder: (context, child) {
        return GetMaterialApp(
          theme: ThemeData(
              scaffoldBackgroundColor: Style.backGrey,
              appBarTheme: AppBarTheme(
                  iconTheme: IconThemeData(color: Style.white),
                  backgroundColor: Style.green,
                  titleTextStyle: TextStyle(
                      color: Style.white,
                      fontSize: Style.fontSizeEighteen.sp))),
          debugShowCheckedModeBanner: true, // 是否有bug标识
          title: title, // APP名称
          translations: Locales(), // 国际化 设置的为我们继承Translations的类
          // locale: Get.deviceLocale, // 设置的设备语言 默认跟随系统
          locale: Locale(store.language.value), // 设置的设备语言
          fallbackLocale: const Locale('zh', 'CN'), // 设置当配置错误时使用的语言
          initialRoute: initialRoute, // 默认路由
          defaultTransition: animation, // 默认动画
          getPages: routes, // 路由数据
          navigatorKey: navigatorKey, // 注册全局context对象
          navigatorObservers: [routeObserver], // 注册路由监听
        );
      },
    );
  }
}
