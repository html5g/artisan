import 'package:get/get.dart';
// 路由
import '/pages/middle/wait.dart';
import '/pages/middle/privacy.dart';
import '/pages/index/index.dart';
import '/pages/login/login.dart';
import '/pages/login/perfect.dart';
import '/pages/login/company.dart';
import '/pages/camera/camera.dart';

// 中间件
import '/cupertino/cupertino.dart';

List<GetPage<dynamic>> routes = [
  GetPage(name: '/', page: () => const Wait()),
  GetPage(
      name: '/index', page: () => const Index(), middlewares: [Cupertino()]),
  GetPage(
    name: '/login',
    page: () => const Login(),
  ),
  GetPage(
    name: '/perfect',
    page: () => const Perfect(),
  ),
  GetPage(
    name: '/company',
    page: () => const Company(),
  ),
  GetPage(
    name: '/privacy',
    page: () => const Privacy(),
  ),
  GetPage(
    name: '/camera',
    page: () => const Camera(),
  ),
];
