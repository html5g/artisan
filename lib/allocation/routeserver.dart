/// 创建一个全局路由监听
library;

import 'package:flutter/material.dart';

RouteObserver<PageRoute> routeObserver = RouteObserver<PageRoute>();
// final MyRouteObserver<PageRoute> routeObserver = MyRouteObserver<PageRoute>();

// class MyRouteObserver<R extends Route<dynamic>> extends RouteObserver<R> {
//   /// 进栈时回调
//   @override
//   void didPush(Route route, Route? previousRoute) {
//     super.didPush(route, previousRoute);
//     print(
//         'didPush route: ${route.settings.name},  previousRoute:${previousRoute?.settings.name}');
//   }

//   /// 出栈时回调
//   @override
//   void didPop(Route route, Route? previousRoute) {
//     super.didPop(route, previousRoute);
//     print(
//         'didPop route: ${route.settings.arguments},  previousRoute:${previousRoute?.settings.name}');
//   }

//   /// 替换路由时回调
//   @override
//   void didReplace({Route? newRoute, Route? oldRoute}) {
//     super.didReplace(newRoute: newRoute, oldRoute: oldRoute);
//     print('didReplace newRoute: $newRoute,oldRoute:$oldRoute');
//   }

//   ///  移除某个路由回调
//   @override
//   void didRemove(Route route, Route? previousRoute) {
//     super.didRemove(route, previousRoute);
//     print('didRemove route: $route,previousRoute:$previousRoute');
//   }

//   ///iOS侧边手势滑动触发回调 手势开始时回调
//   @override
//   void didStartUserGesture(Route route, Route? previousRoute) {
//     super.didStartUserGesture(route, previousRoute);
//     print('didStartUserGesture route: $route,previousRoute:$previousRoute');
//   }

//   ///iOS侧边手势滑动触发停止时回调 不管页面是否退出了都会调用
//   @override
//   void didStopUserGesture() {
//     super.didStopUserGesture();
//     print('didStopUserGesture');
//   }
// }
