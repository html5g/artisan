import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import '/storage/store.dart';

class Cupertino extends GetMiddleware {
  @override
  RouteSettings? redirect(String? route) {
    Store store = Get.put(Store()); // 实例化vuex(实例化控制器)
    if (!store.userInfo.containsKey('id')) {
      return const RouteSettings(name: '/login');
    }
    return null;
  }
}
