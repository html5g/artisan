import 'package:flutter/material.dart';

/// icons图标;

class ModelIcon {
  /// 叉叉图标
  static IconData clear = Icons.clear;

  /// 拍照
  static IconData cameraAlt = Icons.camera_alt;

  /// 闪光灯开启
  static IconData flashOn = Icons.flash_on_rounded;

  /// 闪光灯关闭
  static IconData flashOff = Icons.flash_off_rounded;

  /// 闪光灯自动
  static IconData flashAuto = Icons.flash_auto_rounded;

  /// 闪光灯长亮
  static IconData flashLight = Icons.highlight;

  /// 切换摄像头
  static IconData autorenew = Icons.autorenew_rounded;
}
