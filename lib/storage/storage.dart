import 'package:shared_preferences/shared_preferences.dart';

class Storage {
  // 记录信息List
  static Future setStringList(String key, Map data) async {
    final prefs = await SharedPreferences.getInstance();
    final dataMap = data.keys.map((key) => '$key=${data[key]}').toList();
    return prefs.setStringList(key, dataMap);
  }

  // 读取信息List
  static Future getStringList(String key) async {
    final prefs = await SharedPreferences.getInstance();
    final List<String> pairs = prefs.getStringList(key) ?? [];
    final Map map = Map.fromEntries(pairs.map((pair) {
      final idx = pair.indexOf('=');
      if (idx >= 0) {
        return MapEntry(pair.substring(0, idx), pair.substring(idx + 1));
      } else {
        return MapEntry(pair, '');
      }
    }));
    getAllKeys();
    return map;
  }

  // 存储String
  static Future setString(String key, String data) async {
    final prefs = await SharedPreferences.getInstance();
    return prefs.setString(key, data);
  }

  // 读取String
  static Future getString(String key) async {
    final prefs = await SharedPreferences.getInstance();
    return prefs.getString(key);
  }

  // 存储Bool
  static Future setBool(String key, bool data) async {
    final prefs = await SharedPreferences.getInstance();
    return prefs.setBool(key, data);
  }

  // 读取Bool
  static Future getBool(String key) async {
    final prefs = await SharedPreferences.getInstance();
    return prefs.getBool(key);
  }

  // 读取所有的存储
  static Future<Map<String, dynamic>> getAllKeys() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    Set<String> allKeys = prefs.getKeys();
    Map<String, dynamic> allValue = {};
    for (var key in allKeys) {
      var keyValue = prefs.get(key);
      if (keyValue is List) {
        keyValue = Map.fromEntries(keyValue.map((pair) {
          final idx = pair.indexOf('=');
          if (idx >= 0) {
            return MapEntry(pair.substring(0, idx), pair.substring(idx + 1));
          } else {
            return MapEntry(pair, '');
          }
        }));
      }
      allValue[key] = keyValue;
    }
    return allValue;
  }
}
