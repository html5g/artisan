import 'package:get/get.dart';
import '/storage/storage.dart';

/// Getx记录基本信息数据
///
class Store extends GetxController {
  // 计数时间
  int count = 6;
  int codeCount = 2;
  // tabs默认项
  RxInt taberIndex = 0.obs;
  // 用户信息
  RxMap userInfo = {}.obs;
  // token信息
  RxString token = ''.obs;
  // 刷新token信息
  RxString refreshToken = ''.obs;
  // 是否同意隐私协议
  RxBool privacy = false.obs;
  // 语言
  RxString language = 'zh_CN'.obs;
  // 权限记录第一次
  RxMap permission = {}.obs;

  /// 初始化数据
  Future initSystemInfo() async {
    return await Storage.getAllKeys().then((mapValue) {
      mapValue.forEach((key, value) {
        if (key == 'user_info') userInfo.value = value;
        if (key == 'permission_handler') permission.value = value;
        if (key == 'token') token.value = value;
        if (key == 'refresh_token') refreshToken.value = value;
        if (key == 'privacy') privacy.value = value;
      });
    });
  }

  // 记录用户信息
  void setUserInfo(Map data) {
    userInfo.value = data;
    Storage.setStringList('user_info', data);
  }

  // 记录token信息
  void setToken(String data) {
    token.value = data;
    Storage.setString('token', data);
  }

  // 记录刷新token
  void setRefreshToken(String data) {
    refreshToken.value = data;
    Storage.setString('refresh_token', data);
  }

  // 记录是否选择了隐私协议
  void setPrivacy(bool data) {
    privacy.value = data;
    Storage.setBool('privacy', data);
  }

  // 记录系统获取权限
  void setPermission() {
    Storage.setStringList('permission_handler', permission);
  }
}
