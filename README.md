# 绿端flutter

A new Flutter project.

## 运行
flutter run  --dart-define=DART_DEFINE_APP_ENV=debug    //  开发
flutter run  --dart-define=DART_DEFINE_APP_ENV=test     //  测试
flutter run  --dart-define=DART_DEFINE_APP_ENV=pre      //  灰度
flutter run  --dart-define=DART_DEFINE_APP_ENV=release  //  生产

## 打包
flutter build  --dart-define=DART_DEFINE_APP_ENV=debug    //  开发
flutter build  --dart-define=DART_DEFINE_APP_ENV=test     //  测试
flutter build  --dart-define=DART_DEFINE_APP_ENV=pre      //  灰度
flutter build  --dart-define=DART_DEFINE_APP_ENV=release  //  生产
Android：flutter build apk ...
IOS：flutter build ios ...
flutter build apk --build-name=1.0.0 --build-number=123
